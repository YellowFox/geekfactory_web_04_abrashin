package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;
import ru.geekfactory.homefinance.dao.repository.CategoriesRepository;
import ru.geekfactory.homefinance.service.converters.CategoriesConverter;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class CategoryServiceTest {
    @Mock
    private CategoriesRepository categoriesRepositoryMock;

    @Mock
    private CategoriesConverter categoriesConverterMock;

    @InjectMocks
    private CategoriesService categoriesService;

    @Test
    @DisplayName("Categories repository, findById()")
    void findById() {
        Optional<CategoriesServiceDto> expectedCategoriesModel = Optional.of(new CategoriesServiceDto(1L, "Еда"));

        Mockito.when(categoriesConverterMock.repCatToServiceCatOpt(Mockito.any())).thenReturn(expectedCategoriesModel);

        Optional<CategoriesServiceDto> actualCategoriesModel = categoriesService.findById(1L);
        Assertions.assertEquals(expectedCategoriesModel, actualCategoriesModel);

        Mockito.verify(categoriesConverterMock, Mockito.times(1)).repCatToServiceCatOpt(Mockito.any());
    }

    @Test
    @DisplayName("Categories repository, findAll()")
    void findAll() {
        Collection<CategoriesServiceDto> expectedCategoriesModelCollection = new ArrayList<>();
        expectedCategoriesModelCollection.add(new CategoriesServiceDto(1L, "Еда"));
        expectedCategoriesModelCollection.add(new CategoriesServiceDto(2L, "Квартира"));

        Mockito.when(categoriesConverterMock.repCatCollToServiceCatColl(Mockito.anyCollection())).thenReturn(expectedCategoriesModelCollection);

        Collection<CategoriesServiceDto> actualFoundCollection = categoriesService.findAll();
        Assertions.assertEquals(expectedCategoriesModelCollection, actualFoundCollection);

        Mockito.verify(categoriesConverterMock, Mockito.times(1)).repCatCollToServiceCatColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Categories repository, remove()")
    void remove() {
        Mockito.when(categoriesRepositoryMock.remove(1L)).thenReturn(true);

        boolean result = categoriesService.remove(1L);

        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("Categories repository, save()")
    void save() {
        CategoriesServiceDto expectedCategoriesModel = new CategoriesServiceDto(1L, "Еда");
        Mockito.when(categoriesConverterMock.repCatToServiceCat(categoriesConverterMock.serviceCatToCatRep(expectedCategoriesModel))).thenReturn(expectedCategoriesModel);

        Assertions.assertNotNull(categoriesService);
        CategoriesServiceDto actualCategoriesModel = categoriesService.save(expectedCategoriesModel);
        Assertions.assertEquals(expectedCategoriesModel, actualCategoriesModel);
        Mockito.verify(categoriesConverterMock, Mockito.times(2)).repCatToServiceCat(categoriesConverterMock.serviceCatToCatRep(expectedCategoriesModel));
    }

    @Test
    @DisplayName("Categories repository, update()")
    void update() {
        CategoriesServiceDto categoriesServiceDto = new CategoriesServiceDto(1L, "Еда");
        CategoriesModel categoriesModel = new CategoriesModel(1L, "Еда");

        Mockito.when(categoriesConverterMock.serviceCatToCatRep(categoriesServiceDto)).thenReturn(categoriesModel);
        Mockito.when(categoriesRepositoryMock.update(categoriesModel)).thenReturn(true);

        Assertions.assertTrue(categoriesService.update(categoriesServiceDto));
        Mockito.verify(categoriesConverterMock, Mockito.times(1)).serviceCatToCatRep(categoriesServiceDto);
    }
}
