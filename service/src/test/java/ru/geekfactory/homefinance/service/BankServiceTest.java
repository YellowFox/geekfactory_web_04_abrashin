package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.repository.BankRepository;
import ru.geekfactory.homefinance.service.converters.BankConverter;
import ru.geekfactory.homefinance.service.dto.BankServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class BankServiceTest {

    @Mock
    private BankRepository bankRepositoryMock;

    @Mock
    private BankConverter bankConverterMock;

    @InjectMocks
    private BankService bankService;

    @Test
    @DisplayName("Bank repository, findById()")
    void findById() {
        Optional<BankServiceDto> expectedOptionalBankServiceDto = Optional.of(new BankServiceDto(2L, "SBERBANK"));
        Mockito.when(bankConverterMock.repBankToServiceBankOpt(Mockito.any())).thenReturn(expectedOptionalBankServiceDto);

        Optional<BankServiceDto> actualOptionalBankServiceDto = bankService.findById(2L);

        Assertions.assertEquals(expectedOptionalBankServiceDto, actualOptionalBankServiceDto);
        Mockito.verify(bankConverterMock, Mockito.times(1))
                .repBankToServiceBankOpt(Mockito.any());
    }

    @Test
    @DisplayName("Bank repository, findAll()")
    void findAll() {
        Collection<BankServiceDto> expectedBankModelCollection = new ArrayList<>();
        expectedBankModelCollection.add(new BankServiceDto(1L, "SBERBANK"));
        expectedBankModelCollection.add(new BankServiceDto(2L, "VTB"));
        Mockito.when(bankConverterMock.repBankCollToServiceBankColl(Mockito.anyCollection())).thenReturn(expectedBankModelCollection);

        Collection<BankServiceDto> actualFoundCollection = bankService.findAll();
        Assertions.assertEquals(expectedBankModelCollection, actualFoundCollection);
        Mockito.verify(bankConverterMock, Mockito.times(1)).repBankCollToServiceBankColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Bank repository, remove()")
    void remove() {
        Long id = 1L;
        Mockito.when(bankRepositoryMock.remove(Mockito.anyLong())).thenReturn(true);
        boolean result = bankService.remove(id);
        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("Bank repository, save()")
    void save() {
        BankServiceDto expectedBankModel = new BankServiceDto(1L, "SBERBANK");

        Mockito.when(bankConverterMock.repBankToServiceBank(bankConverterMock.serviceBankToBankRep(expectedBankModel)))
                .thenReturn(expectedBankModel);

        BankServiceDto actualBankModel = bankService.save(expectedBankModel);
        Assertions.assertEquals(expectedBankModel, actualBankModel);

        Mockito.verify(bankConverterMock, Mockito.times(2)).repBankToServiceBank(bankConverterMock.serviceBankToBankRep(expectedBankModel));
    }

    @Test
    @DisplayName("Bank repository, update()")
    void update() {
        BankServiceDto bankServiceDto = new BankServiceDto(1L, "SBERBANK");
        BankModel bankModel = new BankModel(1L, "SBERBANK");

        Mockito.when(bankConverterMock.serviceBankToBankRep((bankServiceDto))).thenReturn(bankModel);
        Mockito.when(bankRepositoryMock.update((bankModel))).thenReturn(true);

        Assertions.assertTrue(bankService.update(bankServiceDto));
        Mockito.verify(bankConverterMock, Mockito.times(1)).serviceBankToBankRep(bankServiceDto);
    }
}
