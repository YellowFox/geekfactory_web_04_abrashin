package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.OperationsModel;
import ru.geekfactory.homefinance.dao.repository.OperationsRepository;
import ru.geekfactory.homefinance.service.converters.OperationConverter;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class OperationServiceTest {
    @Mock
    private OperationsRepository operationsRepositoryMock;

    @Mock
    private OperationConverter operationConverterMock;

    @Mock
    private AccountService accountServiceMock;

    @InjectMocks
    private OperationsService operationsService;

    @Test
    @DisplayName("Operations repository, findById()")
    void findById() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto account = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        Optional<OperationServiceDto> operationsModel = Optional.of(new OperationServiceDto(1L, account, time, new BigDecimal("111.0"), "Des"));

        Mockito.when(operationConverterMock.repOperToServiceOperOpt(Mockito.any())).thenReturn(operationsModel);
        Optional<OperationServiceDto> operationServiceDto = operationsService.findById(1L);
        Assertions.assertEquals(operationsModel, operationServiceDto);

        Mockito.verify(operationConverterMock, Mockito.times(1)).repOperToServiceOperOpt(Mockito.any());
    }

    @Test
    @DisplayName("Operations repository, findAll()")
    void findAll() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto account = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        Collection<OperationServiceDto> expectedOperationsModelCollection = new ArrayList<>();
        expectedOperationsModelCollection.add(new OperationServiceDto(1L, account, time, new BigDecimal("111.0"), "Des"));
        expectedOperationsModelCollection.add(new OperationServiceDto(2L, account, time, new BigDecimal("333.0"), "Des2"));

        Mockito.when(operationConverterMock.repOperCollToServiceOperColl(Mockito.anyCollection())).thenReturn(expectedOperationsModelCollection);

        Collection<OperationServiceDto> actualOperationsModelCollection = operationsService.findAll();
        Assertions.assertEquals(expectedOperationsModelCollection, actualOperationsModelCollection);

        Mockito.verify(operationConverterMock, Mockito.times(1)).repOperCollToServiceOperColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Operations repository, remove()")
    void remove() {
        Mockito.doNothing().when(operationsRepositoryMock).deleteById(Mockito.anyLong());
        operationsService.remove(1L);
        Mockito.verify(operationsRepositoryMock, Mockito.times(1)).deleteById(Mockito.anyLong());
    }

    @Test
    @DisplayName("Operations repository, save()")
    void save() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto accountServiceDto = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        Optional<AccountServiceDto> optionalAccountServiceDto = Optional.of(accountServiceDto);
        OperationServiceDto operationServiceDto = new OperationServiceDto(1L, accountServiceDto, time, new BigDecimal("1.0"), "Des");

        Mockito.when(operationConverterMock.repOperToServiceOper(operationConverterMock.serviceOperToOperRep(operationServiceDto))).thenReturn(operationServiceDto);
        Mockito.when(accountServiceMock.findById(operationServiceDto.getAccountModel().getId())).thenReturn(optionalAccountServiceDto);

        OperationServiceDto actualOperationServiceDto = operationsService.save(operationServiceDto);
        Assertions.assertEquals(operationServiceDto, actualOperationServiceDto);

        Mockito.verify(operationConverterMock, Mockito.times(2)).serviceOperToOperRep(operationServiceDto);
    }

    @Test
    @DisplayName("Operations repository, update()")
    void update() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto accountServiceDto = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        AccountModel accountModel = new AccountModel(1L, "Acc1", new BigDecimal(10));
        Optional<AccountServiceDto> optionalAccountServiceDto = Optional.of(accountServiceDto);
        OperationServiceDto operationServiceDto = new OperationServiceDto(1L, accountServiceDto, time, new BigDecimal("1.0"), "Des");
        OperationsModel operationsModel = new OperationsModel(1L, accountModel, time, new BigDecimal("1.0"), "Des");

        Mockito.when(operationConverterMock.serviceOperToOperRep(operationServiceDto)).thenReturn(operationsModel);
        Mockito.when(operationsRepositoryMock.save(operationsModel)).thenReturn(operationsModel);
        Mockito.when(accountServiceMock.findById(operationServiceDto.getAccountModel().getId())).thenReturn(optionalAccountServiceDto);

        Assertions.assertTrue(operationsService.update(operationServiceDto));
        Mockito.verify(operationConverterMock, Mockito.times(1)).serviceOperToOperRep(operationServiceDto);
    }
}
