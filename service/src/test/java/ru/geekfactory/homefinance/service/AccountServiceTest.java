package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;
import ru.geekfactory.homefinance.service.converters.AccountConverter;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class AccountServiceTest {
    @Mock
    private AccountRepository accountRepositoryMock;

    @Mock
    private AccountConverter accountConverterMock;

    @InjectMocks
    private AccountService accountService;

    @Test
    @DisplayName("Account repository, findById()")
    void findById() {
        Optional<AccountServiceDto> expectedAccountModel = Optional.of(new AccountServiceDto(1L, "Account#1", new BigDecimal(10)));

        Mockito.when(accountConverterMock.repAccountToServiceAccountOpt(Mockito.any())).thenReturn(expectedAccountModel);

        Optional<AccountServiceDto> actualAccountModel = accountService.findById(1L);

        Assertions.assertEquals(expectedAccountModel, actualAccountModel);
        Mockito.verify(accountConverterMock, Mockito.times(1)).repAccountToServiceAccountOpt(Mockito.any());
    }

    @Test
    @DisplayName("Account repository, findAll()")
    void findAll() {
        Collection<AccountServiceDto> expectedAccountModelCollection = new ArrayList<>();
        expectedAccountModelCollection.add(new AccountServiceDto(1L, "Account#1", new BigDecimal(10)));
        expectedAccountModelCollection.add(new AccountServiceDto(2L, "Account#2", new BigDecimal(20)));

        Mockito.when(accountConverterMock.repAccountCollToServiceAccountColl(Mockito.anyCollection())).thenReturn(expectedAccountModelCollection);

        Collection<AccountServiceDto> actualAccountModelCollection = accountService.findAll();

        Assertions.assertEquals(expectedAccountModelCollection, actualAccountModelCollection);
        Mockito.verify(accountConverterMock, Mockito.times(1)).repAccountCollToServiceAccountColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Account repository, remove()")
    void remove() {
        Long id = 1L;
        Mockito.when(accountRepositoryMock.delRefInDependentEntities(Mockito.anyLong())).thenReturn(true);
        Mockito.when(accountRepositoryMock.remove(Mockito.anyLong())).thenReturn(true);
        boolean result = accountService.remove(id);
        Assertions.assertTrue(result);

    }

    @Test
    @DisplayName("Account repository, save()")
    void save() {
        AccountServiceDto expectedAccountModel = new AccountServiceDto(1L, "Account#1", new BigDecimal(10));

        Mockito.when(accountConverterMock.repAccountToServiceAccount(Mockito.any())).thenReturn(expectedAccountModel);

        Assertions.assertNotNull(accountService);
        AccountServiceDto actualAccountModel = accountService.save(expectedAccountModel);
        Assertions.assertEquals(expectedAccountModel, actualAccountModel);

        Mockito.verify(accountConverterMock, Mockito.times(1)).serviceAccountToAccountRep(expectedAccountModel);
    }

    @Test
    @DisplayName("Account repository, update()")
    void update() {
        AccountServiceDto accountServiceDto = new AccountServiceDto(1L, "Account#1", new BigDecimal(10));
        AccountModel accountModel = new AccountModel(1L, "Account#1", new BigDecimal(10));

        Mockito.when(accountRepositoryMock.update(accountModel)).thenReturn(true);
        Mockito.when(accountConverterMock.serviceAccountToAccountRep(accountServiceDto)).thenReturn(accountModel);

        Assertions.assertTrue(accountService.update(accountServiceDto));
        Mockito.verify(accountConverterMock, Mockito.times(1)).serviceAccountToAccountRep(accountServiceDto);
    }
}
