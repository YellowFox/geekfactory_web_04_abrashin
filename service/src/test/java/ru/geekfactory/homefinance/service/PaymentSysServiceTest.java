package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.geekfactory.homefinance.dao.model.PaymentSystemModel;
import ru.geekfactory.homefinance.dao.repository.PaymentSysRepository;
import ru.geekfactory.homefinance.service.converters.PaymentSysConverter;
import ru.geekfactory.homefinance.service.dto.PaymentSysServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class PaymentSysServiceTest {

    @Mock
    private PaymentSysRepository paymentSysRepositoryMock;

    @Mock
    private PaymentSysConverter paymentSysConverterMock;

    @InjectMocks
    private PaymentSysService paymentSysService;

    @Test
    @DisplayName("PaymentSystem repository, findById()")
    void findById() {
        PaymentSysServiceDto expectedPaymentSystemModel = new PaymentSysServiceDto(1L, "VISA");

        Mockito.when(paymentSysConverterMock.repPaymentSysToServicePaymentSysOpt(Mockito.any())).thenReturn(Optional.of(expectedPaymentSystemModel));
        PaymentSysServiceDto actualPaymentsSystemModel = paymentSysService.findById(1L).orElse(null);

        Assertions.assertEquals(expectedPaymentSystemModel, actualPaymentsSystemModel);
        Mockito.verify(paymentSysRepositoryMock, Mockito.times(1)).findById(Mockito.anyLong());

    }

    @Test
    @DisplayName("PaymentSystem repository, findAll()")
    void findAll() {
        Collection<PaymentSysServiceDto> paymentSystemModelCollection = new ArrayList<>();
        paymentSystemModelCollection.add(new PaymentSysServiceDto(1L, "VISA"));
        paymentSystemModelCollection.add(new PaymentSysServiceDto(2L, "MIR"));

        Mockito.when(paymentSysConverterMock.repPaymentSysCollToServicePaymentSysColl(Mockito.anyCollection())).thenReturn(paymentSystemModelCollection);

        Assertions.assertNotNull(paymentSysService);
        Collection<PaymentSysServiceDto> foundCollection = paymentSysService.findAll();

        Assertions.assertEquals(paymentSystemModelCollection, foundCollection);
        Mockito.verify(paymentSysRepositoryMock, Mockito.times(1)).findAll();
    }

    @Test
    @DisplayName("PaymentSystem repository, remove()")
    void remove() {
        Long id = 1L;
        Mockito.when(paymentSysRepositoryMock.remove(Mockito.anyLong())).thenReturn(true);
        boolean result = paymentSysService.remove(id);
        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("PaymentSystem repository, save()")
    void save() {
        PaymentSysServiceDto expectedPaymentSystemModel = new PaymentSysServiceDto(1L, "VISA");
        Mockito.when(paymentSysConverterMock.repPaymentSysToServicePaymentSys(Mockito.any())).thenReturn(expectedPaymentSystemModel);

        PaymentSysServiceDto actualPaymentSystemModel = paymentSysService.save(expectedPaymentSystemModel);

        Assertions.assertEquals(expectedPaymentSystemModel, actualPaymentSystemModel);

        Mockito.verify(paymentSysConverterMock, Mockito.times(1))
                .repPaymentSysToServicePaymentSys(paymentSysConverterMock.servicePaymentSysToPaymentSysRep(expectedPaymentSystemModel));
    }

    @Test
    @DisplayName("PaymentSystem repository, update()")
    void update() {
        PaymentSysServiceDto paymentSysServiceDto = new PaymentSysServiceDto(1L, "VISA");
        PaymentSystemModel paymentSystemModel = new PaymentSystemModel(1L, "VISA");
        Mockito.when(paymentSysConverterMock.servicePaymentSysToPaymentSysRep(Mockito.any())).thenReturn(paymentSystemModel);
        Mockito.when(paymentSysRepositoryMock.update(paymentSystemModel)).thenReturn(true);

        Assertions.assertNotNull(paymentSysService);
        Assertions.assertTrue(paymentSysService.update(paymentSysServiceDto));

        Mockito.verify(paymentSysRepositoryMock, Mockito.times(1)).update(paymentSysConverterMock.servicePaymentSysToPaymentSysRep(paymentSysServiceDto));
    }
}
