package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.converters.CurrencyConverter;
import ru.geekfactory.homefinance.service.dto.CurrencyServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class CurrencyServiceTest {
    @Mock
    private CurrencyRepository currencyRepositoryMock;

    @Mock
    private CurrencyConverter currencyConverterMock;

    @InjectMocks
    private CurrencyService currencyService;

    @Test
    @DisplayName("Currency repository, findById()")
    void findById() {

        CurrencyServiceDto expectedCurrencyModel = new CurrencyServiceDto(1L, "dollar", "$");
        Mockito.when(currencyConverterMock.repCurrToServiceCurrOpt(Mockito.any())).thenReturn(Optional.of(expectedCurrencyModel));

        CurrencyServiceDto actualCurrencyModel = currencyService.findById(1L).orElse(null);
        Assertions.assertEquals(expectedCurrencyModel, actualCurrencyModel);

        Mockito.verify(currencyConverterMock, Mockito.times(1)).repCurrToServiceCurrOpt(Mockito.any());
    }

    @Test
    @DisplayName("Currency repository, findAll()")
    void findAll() {
        Collection<CurrencyServiceDto> expectedCurrencyModelCollection = new ArrayList<>();
        expectedCurrencyModelCollection.add(new CurrencyServiceDto(1L, "dollar", "$"));
        expectedCurrencyModelCollection.add(new CurrencyServiceDto(2L, "euro", "Э"));

        Mockito.when(currencyConverterMock.repCurrCollToServiceCurrColl(Mockito.anyCollection())).thenReturn(expectedCurrencyModelCollection);

        Collection<CurrencyServiceDto> actualCurrencyModelCollection = currencyService.findAll();
        Assertions.assertEquals(expectedCurrencyModelCollection, actualCurrencyModelCollection);

        Mockito.verify(currencyConverterMock, Mockito.times(1)).repCurrCollToServiceCurrColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Currency repository, remove()")
    void remove() {
        Long id = 1L;
        Mockito.when(currencyRepositoryMock.remove(Mockito.anyLong())).thenReturn(true);
        boolean result = currencyService.remove(id);
        Assertions.assertTrue(result);

    }

    @Test
    @DisplayName("Currency repository, save()")
    void save() {
        CurrencyServiceDto expectedCurrencyModel = new CurrencyServiceDto(1L, "dollar", "$");

        Mockito.when(currencyConverterMock.repCurrToServiceCurr(Mockito.any())).thenReturn(expectedCurrencyModel);

        CurrencyServiceDto actualCurrencyModel = currencyService.save(expectedCurrencyModel);
        Assertions.assertEquals(expectedCurrencyModel, actualCurrencyModel);

        Mockito.verify(currencyConverterMock, Mockito.times(1)).repCurrToServiceCurr(currencyConverterMock.serviceCurrToCurrRep(expectedCurrencyModel));
    }

    @Test
    @DisplayName("Currency repository, update()")
    void update() {
        CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto(1L, "dollar", "$");
        CurrencyModel currencyModel = new CurrencyModel(1L, "dollar", "$");
        Mockito.when(currencyConverterMock.serviceCurrToCurrRep(currencyServiceDto)).thenReturn(currencyModel);
        Mockito.when(currencyRepositoryMock.update(currencyModel)).thenReturn(true);

        Assertions.assertTrue(currencyService.update(currencyServiceDto));

        Mockito.verify(currencyRepositoryMock, Mockito.times(1)).update(currencyConverterMock.serviceCurrToCurrRep(currencyServiceDto));
    }
}
