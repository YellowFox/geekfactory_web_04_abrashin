package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.EventsModel;
import ru.geekfactory.homefinance.dao.repository.EventsRepository;
import ru.geekfactory.homefinance.service.converters.EventsConverter;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.EventsServiceDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class EventsServiceTest {
    @Mock
    private EventsRepository eventsRepositoryMock;

    @Mock
    private EventsConverter eventsConverterMock;

    @InjectMocks
    private EventsService eventsService;

    @Test
    @DisplayName("Events repository, findById()")
    void findById() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto accountServiceDto = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        Optional<EventsServiceDto> expectedEventsModel = Optional.of(new EventsServiceDto(1L, "Event1", time, accountServiceDto, new BigDecimal("111.0"), true));

        Mockito.when(eventsConverterMock.repEventToServiceEventOpt(Mockito.any())).thenReturn(expectedEventsModel);

        Optional<EventsServiceDto> actualEventsModel = eventsService.findById(1L);
        Assertions.assertEquals(expectedEventsModel, actualEventsModel);

        Mockito.verify(eventsConverterMock, Mockito.times(1)).repEventToServiceEventOpt(Mockito.any());
    }

    @Test
    @DisplayName("Events repository, findAll()")
    void findAll() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto account = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        Collection<EventsServiceDto> expectedEventsModelCollection = new ArrayList<>();
        expectedEventsModelCollection.add(new EventsServiceDto(1L, "Event1", time, account, new BigDecimal("111.0"),true));
        expectedEventsModelCollection.add(new EventsServiceDto(2L, "Event2", time, account, new BigDecimal("222.0"), true));

        Mockito.when(eventsConverterMock.repEventCollToServiceEventColl(Mockito.anyCollection())).thenReturn(expectedEventsModelCollection);

        Collection<EventsServiceDto> actualEventsModelCollection = eventsService.findAll();
        Assertions.assertEquals(expectedEventsModelCollection, actualEventsModelCollection);

        Mockito.verify(eventsConverterMock, Mockito.times(1)).repEventCollToServiceEventColl(Mockito.anyCollection());
    }

    @Test
    @DisplayName("Events repository, remove()")
    void remove() {
        Mockito.doNothing().when(eventsRepositoryMock).deleteById(Mockito.anyLong());

        eventsService.remove(1L);

        Mockito.verify(eventsRepositoryMock, Mockito.times(1)).deleteById(Mockito.anyLong());
    }

    @Test
    @DisplayName("Events repository, save()")
    void save() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto account = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        EventsServiceDto expectedEventsModel = new EventsServiceDto(1L, "Event1", time, account, new BigDecimal("111.0"), true);

        Mockito.when(eventsConverterMock.repEventToServiceEvent(eventsConverterMock.serviceEventToEventRep(expectedEventsModel))).thenReturn(expectedEventsModel);
        EventsServiceDto actualEventsServiceDto = eventsService.save(expectedEventsModel);

        Assertions.assertEquals(expectedEventsModel, actualEventsServiceDto);
        Mockito.verify(eventsConverterMock, Mockito.times(2)).serviceEventToEventRep(expectedEventsModel);
    }

    @Test
    @DisplayName("Events repository, update()")
    void update() {
        LocalDateTime time = LocalDateTime.of(2019, Month.AUGUST, 1, 0, 0, 0);
        AccountServiceDto accountServiceDto = new AccountServiceDto(1L, "Acc1", new BigDecimal(10));
        AccountModel accountModel = new AccountModel(1L, "Acc1", new BigDecimal(10));
        EventsServiceDto eventsServiceDto = new EventsServiceDto(1L, "Event1", time, accountServiceDto, new BigDecimal("111.0"), true);
        EventsModel eventsModel = new EventsModel(1L, "Event1", time, accountModel, new BigDecimal("111.0"));

        Mockito.when(eventsConverterMock.serviceEventToEventRep(eventsServiceDto)).thenReturn(eventsModel);
        Mockito.when(eventsRepositoryMock.save(eventsModel)).thenReturn(eventsModel);

        Assertions.assertTrue(eventsService.update(eventsServiceDto));
        Mockito.verify(eventsConverterMock, Mockito.times(1)).serviceEventToEventRep(eventsServiceDto);
    }
}
