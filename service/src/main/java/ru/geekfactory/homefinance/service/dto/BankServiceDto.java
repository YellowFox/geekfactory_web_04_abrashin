package ru.geekfactory.homefinance.service.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BankServiceDto {

    private Long id;
    private String name;
}
