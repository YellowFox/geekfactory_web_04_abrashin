package ru.geekfactory.homefinance.service.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class CategoriesConverter {

    private final CategoriesConverter categoriesConverter;

    @Autowired
    @Lazy
    public CategoriesConverter(CategoriesConverter categoriesConverter) {
        this.categoriesConverter = categoriesConverter;
    }

    public Optional<CategoriesServiceDto> repCatToServiceCatOpt(Optional<CategoriesModel> catModel) {
        CategoriesServiceDto categoriesServiceDto = new CategoriesServiceDto();
        if (catModel.isPresent()) {
            categoriesServiceDto.setId(catModel.get().getId());
            categoriesServiceDto.setParent(categoriesConverter.repCatToServiceCat(catModel.get().getParent()));
            categoriesServiceDto.setName(catModel.get().getName());
        }
        return Optional.of(categoriesServiceDto);
    }

    public CategoriesServiceDto repCatToServiceCat(CategoriesModel catModel) {
        CategoriesServiceDto catServiceDto = new CategoriesServiceDto();
        if (catModel != null) {
            catServiceDto.setId(catModel.getId());
            catServiceDto.setName(catModel.getName());
            catServiceDto.setParent(categoriesConverter.repCatToServiceCat(catModel.getParent()));
        } else {
            catServiceDto = null;
            return catServiceDto;
        }
        return catServiceDto;
    }

    public Collection<CategoriesServiceDto> repCatCollToServiceCatColl(Collection<CategoriesModel> categoriesModels) {
        Collection<CategoriesServiceDto> categoriesServiceDtoCollection = new ArrayList<>();
        for (CategoriesModel cat : categoriesModels) {
            categoriesServiceDtoCollection.add(new CategoriesServiceDto(cat.getId(), repCatToServiceCat(cat.getParent()), cat.getName()));
        }
        return categoriesServiceDtoCollection;
    }

    public CategoriesModel serviceCatToCatRep(CategoriesServiceDto categoriesServiceDto) {
        CategoriesModel categoriesModel = new CategoriesModel();
        if (categoriesServiceDto != null) {
            if (categoriesServiceDto.getId() != null) {
                categoriesModel.setId(categoriesServiceDto.getId());
                categoriesModel.setName(categoriesServiceDto.getName());
                if (categoriesServiceDto.getParent() != null) {
                    categoriesModel.setParent(categoriesConverter.serviceCatToCatRep((categoriesServiceDto.getParent())));
                }
            } else {
                categoriesModel.setName(categoriesServiceDto.getName());
                if (categoriesServiceDto.getParent() != null) {
                    categoriesModel.setParent(categoriesConverter.serviceCatToCatRep((categoriesServiceDto.getParent())));
                }
            }
        } else {
            categoriesModel = null;
        }
        return categoriesModel;
    }
}
