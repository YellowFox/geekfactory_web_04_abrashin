package ru.geekfactory.homefinance.service.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class AccountServiceDto {

    private Long id;
    private String name;
    private BigDecimal amount;
    private BankServiceDto bankModel;
    private PaymentSysServiceDto paySystemModel;
    private CurrencyServiceDto currencyModel;
    private String accountType;
    private boolean isActive;
    private UserServiceDto userModel;

    public AccountServiceDto(Long id, String name, BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
    }
}
