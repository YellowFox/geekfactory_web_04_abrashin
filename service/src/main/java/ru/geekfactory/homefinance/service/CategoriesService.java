package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;
import ru.geekfactory.homefinance.dao.repository.CategoriesRepository;
import ru.geekfactory.homefinance.service.converters.CategoriesConverter;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class CategoriesService {

    private final CategoriesRepository categoriesRepository;
    private final CategoriesConverter categoriesConverter;

    @Autowired
    public CategoriesService(CategoriesRepository categoriesRepository, CategoriesConverter categoriesConverter) {
        this.categoriesRepository = categoriesRepository;
        this.categoriesConverter = categoriesConverter;
    }

    public Optional<CategoriesServiceDto> findById(Long id) {
        Optional<CategoriesModel> categoriesModel = categoriesRepository.findById(id);
        return categoriesConverter.repCatToServiceCatOpt(categoriesModel);
    }

    public Collection<CategoriesServiceDto> findAll() {
        Collection<CategoriesModel> categoriesModels = categoriesRepository.findAll();
        return categoriesConverter.repCatCollToServiceCatColl(categoriesModels);
    }

    public boolean remove(Long id) {
        return categoriesRepository.remove(id);
    }

    public CategoriesServiceDto save(CategoriesServiceDto model) {

        return categoriesConverter.repCatToServiceCat(categoriesRepository.save(categoriesConverter.serviceCatToCatRep(model)));
    }

    public boolean update(CategoriesServiceDto model) {

        return categoriesRepository.update(categoriesConverter.serviceCatToCatRep(model));
    }
}
