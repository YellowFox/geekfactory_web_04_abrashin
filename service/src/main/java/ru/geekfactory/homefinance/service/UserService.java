package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.geekfactory.homefinance.dao.model.UserModel;
import ru.geekfactory.homefinance.dao.repository.UserRepository;
import ru.geekfactory.homefinance.service.converters.UsersConverter;
import ru.geekfactory.homefinance.service.dto.UserServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UsersConverter usersConverter;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UsersConverter usersConverter) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.usersConverter = usersConverter;
    }

    public Optional<UserServiceDto> findById(Long id) {
        Optional<UserModel> userServiceDtoOptional = userRepository.findById(id);
        return usersConverter.repUserToServiceUserOpt(userServiceDtoOptional);
    }

    public Optional<UserServiceDto> findByLogin(String login) {
        Optional<UserModel> userServiceDtoOptional = userRepository.findByLogin(login);
        return usersConverter.repUserToServiceUserOpt(userServiceDtoOptional);
    }

    public Collection<UserServiceDto> findAll() {
        Collection<UserModel> userModelCollection = userRepository.findAll();
        return usersConverter.repUserCollToServiceUserColl(userModelCollection);
    }

    public boolean remove(Long id) {
        userRepository.deleteById(id);
        return true;
    }

    public UserServiceDto save(UserServiceDto model) {
        if (model.getPassword() != null) {
            model.setPassword(bCryptPasswordEncoder.encode(model.getPassword()));
            return usersConverter.repUserToServiceUser(userRepository.
                    save(usersConverter.serviceUserToUserRep(model)));
        } else {
            return usersConverter.repUserToServiceUser(userRepository.
                    save(usersConverter.serviceUserToUserRep(model)));
        }

    }

    public boolean update(UserServiceDto model) {
        if (model.getPassword() == null) {
            Optional<UserServiceDto> userServiceDto = findById(model.getId());
            if (userServiceDto.isPresent()) {
                userServiceDto.get().setLogin(model.getLogin());
                return userRepository.save(usersConverter.serviceUserToUserRep(userServiceDto.get())) != null;
            }
        }
        if (model.getLogin() == null) {
            Optional<UserServiceDto> userServiceDto = findById(model.getId());
            if (userServiceDto.isPresent()) {
                userServiceDto.get().setPassword(bCryptPasswordEncoder.encode(model.getPassword()));
                return userRepository.save(usersConverter.serviceUserToUserRep(userServiceDto.get())) != null;
            }
        }
        if (model.getPassword() != null) {
            model.setPassword(bCryptPasswordEncoder.encode(model.getPassword()));
            return userRepository.save(usersConverter.serviceUserToUserRep(model)) != null;
        } else {
            return userRepository.save(usersConverter.serviceUserToUserRep(model)) != null;
        }
    }
}
