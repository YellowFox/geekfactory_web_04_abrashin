package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.repository.BankRepository;
import ru.geekfactory.homefinance.service.converters.BankConverter;
import ru.geekfactory.homefinance.service.dto.BankServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class BankService {

    private final BankRepository bankRepository;
    private final BankConverter bankConverter;

    @Autowired
    public BankService(BankRepository bankRepository, BankConverter bankConverter) {
        this.bankRepository = bankRepository;
        this.bankConverter = bankConverter;
    }

    public Optional<BankServiceDto> findById(Long id) {
        Optional<BankModel> bankModel = bankRepository.findById(id);
        return bankConverter.repBankToServiceBankOpt(bankModel);
    }

    public Collection<BankServiceDto> findAll() {
        Collection<BankModel> bankModels = bankRepository.findAll();
        return bankConverter.repBankCollToServiceBankColl(bankModels);
    }

    public boolean remove(Long id) {
        return bankRepository.remove(id);
    }

    public BankServiceDto save(BankServiceDto model) {

        return bankConverter.repBankToServiceBank(bankRepository.save(bankConverter.serviceBankToBankRep(model)));
    }

    public boolean update(BankServiceDto model) {
        return bankRepository.update(bankConverter.serviceBankToBankRep(model));
    }
}
