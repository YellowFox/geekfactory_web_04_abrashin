package ru.geekfactory.homefinance.service.converters;

import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.dto.CurrencyServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class CurrencyConverter {

    public Optional<CurrencyServiceDto> repCurrToServiceCurrOpt(Optional<CurrencyModel> currencyModel) {
        CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto();
        if (currencyModel.isPresent()) {
            currencyServiceDto.setId(currencyModel.get().getId());
            currencyServiceDto.setName(currencyModel.get().getName());
        }
        return Optional.of(currencyServiceDto);
    }

    public CurrencyServiceDto repCurrToServiceCurr(CurrencyModel currencyModel) {
        CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto();
        if (currencyModel != null) {
            currencyServiceDto.setId(currencyModel.getId());
            currencyServiceDto.setName(currencyModel.getName());
            currencyServiceDto.setSymbol(currencyModel.getSymbol());
        } else {
            currencyServiceDto = null;
            return currencyServiceDto;
        }
        return currencyServiceDto;
    }

    public Collection<CurrencyServiceDto> repCurrCollToServiceCurrColl(Collection<CurrencyModel> currencyModels) {
        Collection<CurrencyServiceDto> currencyServiceDtoCollection = new ArrayList<>();
        for (CurrencyModel curr : currencyModels) {
            currencyServiceDtoCollection.add(new CurrencyServiceDto(curr.getId(), curr.getName(), curr.getSymbol()));
        }
        return currencyServiceDtoCollection;
    }

    public CurrencyModel serviceCurrToCurrRep(CurrencyServiceDto currencyServiceDto) {
        CurrencyModel currencyModel = new CurrencyModel();
        if (currencyServiceDto != null) {
            if (currencyServiceDto.getId() != null) {
                currencyModel.setId(currencyServiceDto.getId());
                currencyModel.setName(currencyServiceDto.getName());
                currencyModel.setSymbol(currencyServiceDto.getSymbol());
            } else {
                currencyModel.setName(currencyServiceDto.getName());
                currencyModel.setSymbol(currencyServiceDto.getSymbol());
            }
        } else {
            currencyModel = null;
        }

        return currencyModel;
    }
}
