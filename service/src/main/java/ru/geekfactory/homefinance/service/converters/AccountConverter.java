package ru.geekfactory.homefinance.service.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class AccountConverter {

    private final BankConverter bankConverter;
    private final PaymentSysConverter paymentSysConverter;
    private final CurrencyConverter currencyConverter;
    private final UsersConverter usersConverter;

    @Autowired
    public AccountConverter(BankConverter bankConverter,
                            PaymentSysConverter paymentSysConverter,
                            CurrencyConverter currencyConverter,
                            UsersConverter usersConverter) {
        this.bankConverter = bankConverter;
        this.paymentSysConverter = paymentSysConverter;
        this.currencyConverter = currencyConverter;
        this.usersConverter = usersConverter;
    }

    public Optional<AccountServiceDto> repAccountToServiceAccountOpt(Optional<AccountModel> accountModel) {
        AccountServiceDto accountServiceDto = new AccountServiceDto();
        if (accountModel.isPresent()) {
            accountServiceDto.setId(accountModel.get().getId());
            accountServiceDto.setName(accountModel.get().getName());
            accountServiceDto.setAccountType(accountModel.get().getAccountType());
            accountServiceDto.setAmount(accountModel.get().getAmount());
            accountServiceDto.setBankModel(bankConverter.repBankToServiceBank(accountModel.get().getBankModel()));
            accountServiceDto.setPaySystemModel(paymentSysConverter.repPaymentSysToServicePaymentSys(accountModel.get().getPaySystemModel()));
            accountServiceDto.setCurrencyModel(currencyConverter.repCurrToServiceCurr(accountModel.get().getCurrencyModel()));
            accountServiceDto.setActive(accountModel.get().isActive());
            accountServiceDto.setUserModel(usersConverter.repUserToServiceUser(accountModel.get().getUserModel()));
        }
        return Optional.of(accountServiceDto);
    }

    public AccountServiceDto repAccountToServiceAccount(AccountModel accountModel) {
        AccountServiceDto accountServiceDto = new AccountServiceDto();
        if (accountModel != null) {
            accountServiceDto.setId(accountModel.getId());
            accountServiceDto.setName(accountModel.getName());
            accountServiceDto.setAccountType(accountModel.getAccountType());
            accountServiceDto.setAmount(accountModel.getAmount());
            accountServiceDto.setBankModel(bankConverter.repBankToServiceBank(accountModel.getBankModel()));
            accountServiceDto.setPaySystemModel(paymentSysConverter.repPaymentSysToServicePaymentSys(accountModel.getPaySystemModel()));
            accountServiceDto.setCurrencyModel(currencyConverter.repCurrToServiceCurr(accountModel.getCurrencyModel()));
            accountServiceDto.setActive(accountModel.isActive());
            accountServiceDto.setUserModel(usersConverter.repUserToServiceUser(accountModel.getUserModel()));
        } else {
            accountServiceDto = null;
            return accountServiceDto;
        }
        return accountServiceDto;
    }

    public Collection<AccountServiceDto> repAccountCollToServiceAccountColl(Collection<AccountModel> accountModels) {
        Collection<AccountServiceDto> accountServiceDtoCollection = new ArrayList<>();
        for (AccountModel acc : accountModels) {
            AccountServiceDto accountServiceDto = new AccountServiceDto();
            accountServiceDto.setId(acc.getId());
            accountServiceDto.setName(acc.getName());
            accountServiceDto.setAccountType(acc.getAccountType());
            accountServiceDto.setAmount(acc.getAmount());
            accountServiceDto.setBankModel(bankConverter.repBankToServiceBank(acc.getBankModel()));
            accountServiceDto.setPaySystemModel(paymentSysConverter.repPaymentSysToServicePaymentSys(acc.getPaySystemModel()));
            accountServiceDto.setCurrencyModel(currencyConverter.repCurrToServiceCurr(acc.getCurrencyModel()));
            accountServiceDto.setActive(acc.isActive());
            accountServiceDto.setUserModel(usersConverter.repUserToServiceUser(acc.getUserModel()));
            accountServiceDtoCollection.add(accountServiceDto);
        }
        return accountServiceDtoCollection;
    }

    public Collection<AccountModel> serviceAccountCollToRepAccountColl(Collection<AccountServiceDto> accountServiceDtos) {
        Collection<AccountModel> accountModelCollection = new ArrayList<>();
        for (AccountServiceDto acc : accountServiceDtos) {
            AccountModel accountModel = new AccountModel();
            accountModel.setId(acc.getId());
            accountModel.setName(acc.getName());
            accountModel.setAccountType(acc.getAccountType());
            accountModel.setAmount(acc.getAmount());
            accountModel.setBankModel(bankConverter.serviceBankToBankRep(acc.getBankModel()));
            accountModel.setPaySystemModel(paymentSysConverter.servicePaymentSysToPaymentSysRep(acc.getPaySystemModel()));
            accountModel.setCurrencyModel(currencyConverter.serviceCurrToCurrRep(acc.getCurrencyModel()));
            accountModel.setActive(acc.isActive());
            accountModel.setUserModel(usersConverter.serviceUserToUserRep(acc.getUserModel()));
            accountModelCollection.add(accountModel);
        }
        return accountModelCollection;
    }

    public AccountModel serviceAccountToAccountRep(AccountServiceDto accountServiceDto) {
        AccountModel accountModel = new AccountModel();
        if (accountServiceDto != null) {
            if (accountServiceDto.getId() != null) {
                accountModel.setId(accountServiceDto.getId());
                accountModel.setName(accountServiceDto.getName());
                accountModel.setAccountType(accountServiceDto.getAccountType());
                accountModel.setAmount(accountServiceDto.getAmount());
                accountModel.setBankModel(bankConverter.serviceBankToBankRep(accountServiceDto.getBankModel()));
                accountModel.setPaySystemModel(paymentSysConverter.servicePaymentSysToPaymentSysRep(accountServiceDto.getPaySystemModel()));
                accountModel.setCurrencyModel(currencyConverter.serviceCurrToCurrRep(accountServiceDto.getCurrencyModel()));
                accountModel.setActive(accountServiceDto.isActive());
                accountModel.setUserModel(usersConverter.serviceUserToUserRep(accountServiceDto.getUserModel()));
            } else {
                accountModel.setName(accountServiceDto.getName());
                accountModel.setAccountType(accountServiceDto.getAccountType());
                accountModel.setAmount(accountServiceDto.getAmount());
                accountModel.setBankModel(bankConverter.serviceBankToBankRep(accountServiceDto.getBankModel()));
                accountModel.setPaySystemModel(paymentSysConverter.servicePaymentSysToPaymentSysRep(accountServiceDto.getPaySystemModel()));
                accountModel.setCurrencyModel(currencyConverter.serviceCurrToCurrRep(accountServiceDto.getCurrencyModel()));
                accountModel.setActive(accountServiceDto.isActive());
                accountModel.setUserModel(usersConverter.serviceUserToUserRep(accountServiceDto.getUserModel()));
            }
        } else {
            accountModel = null;
        }
        return accountModel;
    }
}
