package ru.geekfactory.homefinance.service.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class EventsServiceDto {

    private Long id;
    private String name;
    private LocalDateTime date;
    private AccountServiceDto accountModel;
    private Collection<CategoriesServiceDto> categories;
    private BigDecimal amount;
    private Boolean isActive;

    public EventsServiceDto(Long id, String name, LocalDateTime date, AccountServiceDto accountModel, BigDecimal amount, Boolean isActive) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.accountModel = accountModel;
        this.amount = amount;
        this.isActive = isActive;
    }
}
