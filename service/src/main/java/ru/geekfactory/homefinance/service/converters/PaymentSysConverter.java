package ru.geekfactory.homefinance.service.converters;

import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.PaymentSystemModel;
import ru.geekfactory.homefinance.service.dto.PaymentSysServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class PaymentSysConverter {

    public Optional<PaymentSysServiceDto> repPaymentSysToServicePaymentSysOpt(Optional<PaymentSystemModel> paymentSystemModel) {

        PaymentSysServiceDto paymentSysServiceDto = new PaymentSysServiceDto();
        if (paymentSystemModel.isPresent()) {
            paymentSysServiceDto.setId(paymentSystemModel.get().getId());
            paymentSysServiceDto.setName(paymentSystemModel.get().getName());
        }
        return Optional.of(paymentSysServiceDto);
    }

    public PaymentSysServiceDto repPaymentSysToServicePaymentSys(PaymentSystemModel accountModel) {
        PaymentSysServiceDto paymentSysServiceDto = new PaymentSysServiceDto();
        if (accountModel != null) {
            paymentSysServiceDto.setId(accountModel.getId());
            paymentSysServiceDto.setName(accountModel.getName());
        } else {
            paymentSysServiceDto = null;
            return paymentSysServiceDto;
        }
        return paymentSysServiceDto;
    }

    public Collection<PaymentSysServiceDto> repPaymentSysCollToServicePaymentSysColl(Collection<PaymentSystemModel> paymentSystemModels) {
        Collection<PaymentSysServiceDto> paymentSysServiceDtoCollection = new ArrayList<>();
        for (PaymentSystemModel acc : paymentSystemModels) {
            paymentSysServiceDtoCollection.add(new PaymentSysServiceDto(acc.getId(), acc.getName()));
        }
        return paymentSysServiceDtoCollection;
    }

    public PaymentSystemModel servicePaymentSysToPaymentSysRep(PaymentSysServiceDto paymentSysServiceDto) {
        PaymentSystemModel paymentSystemModel = new PaymentSystemModel();
        if (paymentSysServiceDto != null) {
            if (paymentSysServiceDto.getId() != null) {
                paymentSystemModel.setId(paymentSysServiceDto.getId());
                paymentSystemModel.setName(paymentSysServiceDto.getName());
            } else {
                paymentSystemModel.setName(paymentSysServiceDto.getName());
            }
        } else {
            paymentSystemModel = null;
        }
        return paymentSystemModel;
    }

}
