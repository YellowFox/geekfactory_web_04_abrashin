package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;
import ru.geekfactory.homefinance.service.converters.AccountConverter;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountConverter accountConverter;

    @Autowired
    public AccountService(AccountRepository accountRepository, AccountConverter accountConverter) {
        this.accountRepository = accountRepository;
        this.accountConverter = accountConverter;
    }

    public Optional<AccountServiceDto> findById(Long id) {

        Optional<AccountModel> accountModel = accountRepository.findById(id);
        return accountConverter.repAccountToServiceAccountOpt(accountModel);
    }

    public Collection<AccountServiceDto> findAll() {
        Collection<AccountModel> accountModels = accountRepository.findAll();
        return accountConverter.repAccountCollToServiceAccountColl(accountModels);
    }

    public Collection<AccountServiceDto> findAllAccountsWithId(Long id) {
        Collection<AccountModel> accountModels = accountRepository.findAllAccountsWithId(id);
        return accountConverter.repAccountCollToServiceAccountColl(accountModels);
    }

    public boolean remove(Long id) {
        accountRepository.delRefInDependentEntities(id);
        return accountRepository.remove(id);
    }

    public AccountServiceDto save(AccountServiceDto model) {
        AccountModel accountModel = accountRepository.save(accountConverter.serviceAccountToAccountRep(model));
        return accountConverter.repAccountToServiceAccount(accountModel);
    }

    public boolean update(AccountServiceDto model) {
        return accountRepository.update(accountConverter.serviceAccountToAccountRep(model));
    }
}
