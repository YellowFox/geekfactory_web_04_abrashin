package ru.geekfactory.homefinance.service.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;
import ru.geekfactory.homefinance.dao.model.EventsModel;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.EventsServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class EventsConverter {

    final private AccountConverter accountConverter;
    final private CategoriesConverter categoriesConverter;

    @Autowired
    public EventsConverter(AccountConverter accountConverter,
                           CategoriesConverter categoriesConverter) {
        this.accountConverter = accountConverter;
        this.categoriesConverter = categoriesConverter;
    }

    public Optional<EventsServiceDto> repEventToServiceEventOpt(Optional<EventsModel> eventsModel) {
        EventsServiceDto eventsServiceDto = new EventsServiceDto();
        if (eventsModel.isPresent()) {
            eventsServiceDto.setId(eventsModel.get().getId());
            eventsServiceDto.setName(eventsModel.get().getName());
            eventsServiceDto.setDate(eventsModel.get().getDate());
            eventsServiceDto.setAmount(eventsModel.get().getAmount());
            eventsServiceDto.setIsActive(eventsModel.get().getIsActive());
            eventsServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(eventsModel.get().getAccountModel()));
            Collection<CategoriesServiceDto> eventsServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : eventsModel.get().getCategories()) {
                eventsServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            eventsServiceDto.setCategories(eventsServiceDtos);

        }
        return Optional.of(eventsServiceDto);
    }

    public EventsServiceDto repEventToServiceEvent(EventsModel eventsModel) {
        EventsServiceDto eventsServiceDto = new EventsServiceDto();
        if (eventsModel != null) {
            eventsServiceDto.setId(eventsModel.getId());
            eventsServiceDto.setName(eventsModel.getName());
            eventsServiceDto.setDate(eventsModel.getDate());
            eventsServiceDto.setAmount(eventsModel.getAmount());
            eventsServiceDto.setIsActive(eventsModel.getIsActive());
            eventsServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(eventsModel.getAccountModel()));
            Collection<CategoriesServiceDto> eventsServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : eventsModel.getCategories()) {
                eventsServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            eventsServiceDto.setCategories(eventsServiceDtos);
        } else {
            eventsServiceDto = null;
            return eventsServiceDto;
        }
        return eventsServiceDto;
    }

    public Collection<EventsServiceDto> repEventCollToServiceEventColl(Collection<EventsModel> eventsModels) {
        Collection<EventsServiceDto> eventsServiceDtoCollection = new ArrayList<>();
        for (EventsModel event : eventsModels) {
            EventsServiceDto eventsServiceDto = new EventsServiceDto();
            eventsServiceDto.setId(event.getId());
            eventsServiceDto.setName(event.getName());
            eventsServiceDto.setDate(event.getDate());
            eventsServiceDto.setAmount(event.getAmount());
            eventsServiceDto.setIsActive(event.getIsActive());
            eventsServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(event.getAccountModel()));
            Collection<CategoriesServiceDto> eventsServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : event.getCategories()) {
                eventsServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            eventsServiceDto.setCategories(eventsServiceDtos);
            eventsServiceDtoCollection.add(eventsServiceDto);
        }
        return eventsServiceDtoCollection;
    }

    public EventsModel serviceEventToEventRep(EventsServiceDto eventsServiceDto) {
        EventsModel eventsModel = new EventsModel();
        if (eventsServiceDto.getId() != null) {
            eventsModel.setId(eventsServiceDto.getId());
            eventsModel.setName(eventsServiceDto.getName());
            eventsModel.setDate(eventsServiceDto.getDate());
            eventsModel.setAmount(eventsServiceDto.getAmount());
            eventsModel.setIsActive(eventsServiceDto.getIsActive());
            eventsModel.setAccountModel(accountConverter.serviceAccountToAccountRep(eventsServiceDto.getAccountModel()));
            Collection<CategoriesModel> categoriesModelCollection = new ArrayList<>();
            if (eventsServiceDto.getCategories() != null) {
                for (CategoriesServiceDto cat : eventsServiceDto.getCategories()) {
                    CategoriesModel categoriesModel = new CategoriesModel();
                    categoriesModel.setId(cat.getId());
                    categoriesModel.setName(cat.getName());
                    categoriesModel.setParent(categoriesConverter.serviceCatToCatRep(cat.getParent()));
                    categoriesModelCollection.add(categoriesModel);
                }
            } else {
                eventsModel.setCategories(null);
            }
            eventsModel.setCategories(categoriesModelCollection);
        } else {
            eventsModel.setName(eventsServiceDto.getName());
            eventsModel.setDate(eventsServiceDto.getDate());
            eventsModel.setAmount(eventsServiceDto.getAmount());
            eventsModel.setIsActive(eventsServiceDto.getIsActive());
            eventsModel.setAccountModel(accountConverter.serviceAccountToAccountRep(eventsServiceDto.getAccountModel()));
            Collection<CategoriesModel> categoriesModelCollection = new ArrayList<>();
            for (CategoriesServiceDto cat : eventsServiceDto.getCategories()) {
                CategoriesModel categoriesModel = new CategoriesModel();
                categoriesModel.setId(cat.getId());
                categoriesModel.setName(cat.getName());
                categoriesModel.setParent(categoriesConverter.serviceCatToCatRep(cat.getParent()));
                categoriesModelCollection.add(categoriesModel);
            }
            eventsModel.setCategories(categoriesModelCollection);
        }
        return eventsModel;
    }
}
