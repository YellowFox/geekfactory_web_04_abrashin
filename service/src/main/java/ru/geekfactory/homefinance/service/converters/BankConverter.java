package ru.geekfactory.homefinance.service.converters;


import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.service.dto.BankServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class BankConverter {

    public Optional<BankServiceDto> repBankToServiceBankOpt(Optional<BankModel> bankModel) {
        BankServiceDto bankServiceDto = new BankServiceDto();
        if (bankModel.isPresent()) {
            bankServiceDto.setId(bankModel.get().getId());
            bankServiceDto.setName(bankModel.get().getName());
        }
        return Optional.of(bankServiceDto);
    }

    public BankServiceDto repBankToServiceBank(BankModel bankModel) {
        BankServiceDto bankServiceDto = new BankServiceDto();
        if (bankModel != null) {
            bankServiceDto.setId(bankModel.getId());
            bankServiceDto.setName(bankModel.getName());
        } else {
            bankServiceDto = null;
            return bankServiceDto;
        }
        return bankServiceDto;
    }

    public Collection<BankServiceDto> repBankCollToServiceBankColl(Collection<BankModel> bankModel) {
        Collection<BankServiceDto> bankServiceDtoCollection = new ArrayList<>();
        for (BankModel bank : bankModel) {
            bankServiceDtoCollection.add(new BankServiceDto(bank.getId(), bank.getName()));
        }
        return bankServiceDtoCollection;
    }

    public BankModel serviceBankToBankRep(BankServiceDto bankServiceDto) {
        BankModel bankModel = new BankModel();
        if (bankServiceDto != null) {
            if (bankServiceDto.getId() != null) {
                bankModel.setId(bankServiceDto.getId());
                bankModel.setName(bankServiceDto.getName());
            } else {
                bankModel.setName(bankServiceDto.getName());
            }
        } else {
            bankModel = null;
        }
        return bankModel;
    }
}
