package ru.geekfactory.homefinance.service.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CurrencyServiceDto {
    private Long id;
    private String name;
    private String symbol;
}
