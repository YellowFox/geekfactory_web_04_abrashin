package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.PaymentSystemModel;
import ru.geekfactory.homefinance.dao.repository.PaymentSysRepository;
import ru.geekfactory.homefinance.service.converters.PaymentSysConverter;
import ru.geekfactory.homefinance.service.dto.PaymentSysServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class PaymentSysService {

    private final PaymentSysRepository paymentSysRepository;
    private final PaymentSysConverter paymentSysConverter;

    @Autowired
    public PaymentSysService(PaymentSysRepository paymentSysRepository, PaymentSysConverter paymentSysConverter) {
        this.paymentSysRepository = paymentSysRepository;
        this.paymentSysConverter = paymentSysConverter;
    }

    public Optional<PaymentSysServiceDto> findById(Long id) {
        Optional<PaymentSystemModel> paymentSystemModel = paymentSysRepository.findById(id);
        return paymentSysConverter.repPaymentSysToServicePaymentSysOpt(paymentSystemModel);
    }

    public Collection<PaymentSysServiceDto> findAll() {
        Collection<PaymentSystemModel> paymentSystemModels = paymentSysRepository.findAll();
        return paymentSysConverter.repPaymentSysCollToServicePaymentSysColl(paymentSystemModels);
    }

    public boolean remove(Long id) {
        return paymentSysRepository.remove(id);
    }

    public PaymentSysServiceDto save(PaymentSysServiceDto model) {

        return paymentSysConverter.repPaymentSysToServicePaymentSys(paymentSysRepository.save(paymentSysConverter.servicePaymentSysToPaymentSysRep(model)));
    }

    public boolean update(PaymentSysServiceDto model) {

        return paymentSysRepository.update(paymentSysConverter.servicePaymentSysToPaymentSysRep(model));
    }
}
