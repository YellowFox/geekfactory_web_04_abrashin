package ru.geekfactory.homefinance.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.geekfactory.homefinance.dao.config.DaoConfiguration;

@Configuration
@Import(DaoConfiguration.class)
@ComponentScan("ru.geekfactory.homefinance.service")
public class ServiceConfiguration {
}
