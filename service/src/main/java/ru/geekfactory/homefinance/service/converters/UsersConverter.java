package ru.geekfactory.homefinance.service.converters;

import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.UserModel;
import ru.geekfactory.homefinance.service.dto.UserServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class UsersConverter {

    public Optional<UserServiceDto> repUserToServiceUserOpt(Optional<UserModel> userModel) {
        UserServiceDto userServiceDto = new UserServiceDto();
        if (userModel.isPresent()) {
            userServiceDto.setId(userModel.get().getId());
            userServiceDto.setLogin(userModel.get().getLogin());
            userServiceDto.setPassword(userModel.get().getPassword());
            userServiceDto.setRole(userModel.get().getRole());
        } else {
            return Optional.empty();
        }
        return Optional.of(userServiceDto);
    }

    public UserServiceDto repUserToServiceUser(UserModel userModel) {
        UserServiceDto userServiceDto = new UserServiceDto();
        if (userModel != null) {
            userServiceDto.setId(userModel.getId());
            userServiceDto.setLogin(userModel.getLogin());
            userServiceDto.setPassword(userModel.getPassword());
            userServiceDto.setRole(userModel.getRole());
            return userServiceDto;
        } else {
            return null;
        }
    }

    public Collection<UserServiceDto> repUserCollToServiceUserColl(Collection<UserModel> userModels) {
        Collection<UserServiceDto> userServiceDtoCollection = new ArrayList<>();
        for (UserModel userModel : userModels) {
            UserServiceDto userServiceDto = new UserServiceDto();
            userServiceDto.setId(userModel.getId());
            userServiceDto.setLogin(userModel.getLogin());
            userServiceDto.setPassword(userModel.getPassword());
            userServiceDto.setRole(userModel.getRole());
            userServiceDtoCollection.add(userServiceDto);
        }
        return userServiceDtoCollection;
    }

    public UserModel serviceUserToUserRep(UserServiceDto userServiceDto) {
        if (userServiceDto != null) {
            UserModel userModel = new UserModel();
            userModel.setId(userServiceDto.getId());
            userModel.setLogin(userServiceDto.getLogin());
            userModel.setPassword(userServiceDto.getPassword());
            userModel.setRole(userServiceDto.getRole());
            return userModel;
        } else {
            return null;
        }
    }
}