package ru.geekfactory.homefinance.service.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CategoriesServiceDto {

    private Long id;
    private CategoriesServiceDto parent;
    private String name;

    public CategoriesServiceDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}


