package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.exceptions.NotEnoughMoneyException;
import ru.geekfactory.homefinance.dao.exceptions.ObjectNotFoundException;
import ru.geekfactory.homefinance.dao.model.OperationsModel;
import ru.geekfactory.homefinance.dao.repository.OperationsRepository;
import ru.geekfactory.homefinance.service.converters.AccountConverter;
import ru.geekfactory.homefinance.service.converters.OperationConverter;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class OperationsService {

    private final OperationsRepository operationsRepository;
    private final AccountService accountService;
    private final OperationConverter operationConverter;
    private final AccountConverter accountConverter;

    @Autowired
    public OperationsService(OperationsRepository operationsRepository, AccountService accountService, OperationConverter operationConverter, AccountConverter accountConverter) {
        this.operationsRepository = operationsRepository;
        this.accountService = accountService;
        this.operationConverter = operationConverter;
        this.accountConverter = accountConverter;
    }

    public Optional<OperationServiceDto> findById(Long id) {
        Optional<OperationsModel> operationServiceDto = operationsRepository.findById(id);
        return operationConverter.repOperToServiceOperOpt(operationServiceDto);
    }

    public Collection<OperationServiceDto> findByAccountModelById(Collection<AccountServiceDto> accountModels) {
        Collection<OperationsModel> operationsModels = operationsRepository.findAllByAccountModelIsIn(accountConverter.serviceAccountCollToRepAccountColl(accountModels));
        return operationConverter.repOperCollToServiceOperColl(operationsModels);
    }

    public Collection<OperationServiceDto> findAll() {
        Collection<OperationsModel> operationsModels = operationsRepository.findAll();
        return operationConverter.repOperCollToServiceOperColl(operationsModels);
    }

    public boolean remove(Long id) {
        operationsRepository.deleteById(id);
        return true;
    }

    public OperationServiceDto save(OperationServiceDto model) {
        balanceAccountCheck(model);
        return operationConverter.repOperToServiceOper(operationsRepository.save(operationConverter.serviceOperToOperRep(model)));
    }

    public boolean update(OperationServiceDto model) {
        balanceAccountCheck(model);
        return operationsRepository.save(operationConverter.serviceOperToOperRep(model)) != null;
    }

    public void balanceAccountCheck(OperationServiceDto model) {
        Optional<AccountServiceDto> accountServiceDto = accountService.findById(model.getAccountModel().getId());
        if (accountServiceDto.isPresent()) {
            BigDecimal amountAccount = accountServiceDto.get().getAmount();
            BigDecimal amountOperation = model.getAmount();
            if (amountOperation.compareTo(amountAccount) > 0) {
                throw new NotEnoughMoneyException("not enough money to account");
            }
            accountServiceDto.get().setAmount(amountAccount.subtract(amountOperation));
            accountService.update(accountServiceDto.get());
        } else {
            throw new ObjectNotFoundException("account not found");
        }
    }
}
