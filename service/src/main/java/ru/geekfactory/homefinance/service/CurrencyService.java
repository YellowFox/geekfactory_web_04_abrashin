package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.converters.CurrencyConverter;
import ru.geekfactory.homefinance.service.dto.CurrencyServiceDto;

import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final CurrencyConverter currencyConverter;

    @Autowired
    public CurrencyService(CurrencyRepository currencyRepository, CurrencyConverter currencyConverter) {
        this.currencyRepository = currencyRepository;
        this.currencyConverter = currencyConverter;
    }

    public Optional<CurrencyServiceDto> findById(Long id) {
        Optional<CurrencyModel> optionalCurrencyModel = currencyRepository.findById(id);
        return currencyConverter.repCurrToServiceCurrOpt(optionalCurrencyModel);
    }

    public Collection<CurrencyServiceDto> findAll() {
        Collection<CurrencyModel> currencyModelCollection = currencyRepository.findAll();
        return currencyConverter.repCurrCollToServiceCurrColl(currencyModelCollection);
    }

    public boolean remove(Long id) {
        return currencyRepository.remove(id);
    }

    public CurrencyServiceDto save(CurrencyServiceDto model) {

        return currencyConverter.repCurrToServiceCurr(currencyRepository.save(currencyConverter.serviceCurrToCurrRep(model)));
    }

    public boolean update(CurrencyServiceDto model) {

        return currencyRepository.update(currencyConverter.serviceCurrToCurrRep(model));
    }
}
