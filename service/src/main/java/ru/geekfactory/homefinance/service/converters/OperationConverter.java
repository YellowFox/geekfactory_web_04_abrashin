package ru.geekfactory.homefinance.service.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;
import ru.geekfactory.homefinance.dao.model.OperationsModel;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
public class OperationConverter {

    @Autowired
    private final AccountConverter accountConverter;
    private final CategoriesConverter categoriesConverter;

    @Autowired
    public OperationConverter(AccountConverter accountConverter,
                              CategoriesConverter categoriesConverter) {
        this.accountConverter = accountConverter;
        this.categoriesConverter = categoriesConverter;
    }

    public Optional<OperationServiceDto> repOperToServiceOperOpt(Optional<OperationsModel> operationsModel) {
        OperationServiceDto operationServiceDto = new OperationServiceDto();
        if (operationsModel.isPresent()) {
            operationServiceDto.setId(operationsModel.get().getId());
            operationServiceDto.setDescription(operationsModel.get().getDescription());
            operationServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(operationsModel.get().getAccountModel()));
            operationServiceDto.setAmount(operationsModel.get().getAmount());
            operationServiceDto.setDate(operationsModel.get().getDate());
            Collection<CategoriesServiceDto> categoriesServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : operationsModel.get().getCategories()) {
                categoriesServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            operationServiceDto.setCategories(categoriesServiceDtos);
        }
        return Optional.of(operationServiceDto);
    }

    public OperationServiceDto repOperToServiceOper(OperationsModel operationsModel) {
        OperationServiceDto operationServiceDto = new OperationServiceDto();
        if (operationsModel != null) {
            operationServiceDto.setId(operationsModel.getId());
            operationServiceDto.setDescription(operationsModel.getDescription());
            operationServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(operationsModel.getAccountModel()));
            operationServiceDto.setAmount(operationsModel.getAmount());
            operationServiceDto.setDate(operationsModel.getDate());
            Collection<CategoriesServiceDto> categoriesServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : operationsModel.getCategories()) {
                categoriesServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            operationServiceDto.setCategories(categoriesServiceDtos);
        } else {
            operationServiceDto = null;
            return operationServiceDto;
        }
        return operationServiceDto;
    }

    public Collection<OperationServiceDto> repOperCollToServiceOperColl(Collection<OperationsModel> operationsModels) {
        Collection<OperationServiceDto> operationServiceCollections = new ArrayList<>();
        for (OperationsModel operationsModel : operationsModels) {
            OperationServiceDto operationServiceDto = new OperationServiceDto();
            operationServiceDto.setId(operationsModel.getId());
            operationServiceDto.setDescription(operationsModel.getDescription());
            operationServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(operationsModel.getAccountModel()));
            operationServiceDto.setAmount(operationsModel.getAmount());
            operationServiceDto.setDate(operationsModel.getDate());
            Collection<CategoriesServiceDto> categoriesServiceDtos = new ArrayList<>();
            for (CategoriesModel cat : operationsModel.getCategories()) {
                categoriesServiceDtos.add(new CategoriesServiceDto(cat.getId(),
                        categoriesConverter.repCatToServiceCat(cat.getParent()), cat.getName()));
            }
            operationServiceDto.setCategories(categoriesServiceDtos);
            operationServiceCollections.add(operationServiceDto);
        }
        return operationServiceCollections;
    }

    public OperationsModel serviceOperToOperRep(OperationServiceDto operationServiceDto) {
        OperationsModel operationsModel = new OperationsModel();
        if (operationServiceDto.getId() != null) {
            operationsModel.setId(operationServiceDto.getId());
            operationsModel.setDescription(operationServiceDto.getDescription());
            operationsModel.setAccountModel(accountConverter.serviceAccountToAccountRep(operationServiceDto.getAccountModel()));
            operationsModel.setAmount(operationServiceDto.getAmount());
            operationsModel.setDate(operationServiceDto.getDate());
            Collection<CategoriesModel> categoriesModelCollection = new ArrayList<>();
            if (operationServiceDto.getCategories() != null) {
                for (CategoriesServiceDto cat : operationServiceDto.getCategories()) {
                    CategoriesModel categoriesModel = new CategoriesModel();
                    categoriesModel.setId(cat.getId());
                    categoriesModel.setName(cat.getName());
                    categoriesModel.setParent(categoriesConverter.serviceCatToCatRep(cat.getParent()));
                    categoriesModelCollection.add(categoriesModel);
                }
                operationsModel.setCategories(categoriesModelCollection);
            } else {
                operationsModel.setCategories(null);
            }
        } else {
            operationsModel.setDescription(operationServiceDto.getDescription());
            operationsModel.setAccountModel(accountConverter.serviceAccountToAccountRep(operationServiceDto.getAccountModel()));
            operationsModel.setAmount(operationServiceDto.getAmount());
            operationsModel.setDate(operationServiceDto.getDate());
            Collection<CategoriesModel> categoriesModelCollection = new ArrayList<>();
            for (CategoriesServiceDto cat : operationServiceDto.getCategories()) {
                CategoriesModel categoriesModel = new CategoriesModel();
                categoriesModel.setId(cat.getId());
                categoriesModel.setName(cat.getName());
                categoriesModel.setParent(categoriesConverter.serviceCatToCatRep(cat.getParent()));
                categoriesModelCollection.add(categoriesModel);
            }
            operationsModel.setCategories(categoriesModelCollection);
        }
        return operationsModel;
    }
}
