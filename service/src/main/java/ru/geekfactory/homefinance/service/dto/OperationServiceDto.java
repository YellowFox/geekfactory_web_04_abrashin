package ru.geekfactory.homefinance.service.dto;

import lombok.*;
import ru.geekfactory.homefinance.dao.model.AccountModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OperationServiceDto {

    private Long id;
    private String description;
    private AccountServiceDto accountModel;
    private BigDecimal amount;
    private LocalDateTime date;
    private Collection<CategoriesServiceDto> categories;

    public OperationServiceDto(Long id, AccountServiceDto accountModel, LocalDateTime date, BigDecimal amount, String description) {
        this.id = id;
        this.accountModel = accountModel;
        this.date = date;
        this.amount = amount;
        this.description = description;
    }
}
