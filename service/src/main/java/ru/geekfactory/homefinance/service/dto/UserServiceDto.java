package ru.geekfactory.homefinance.service.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserServiceDto {

    private Long id;
    private String login;
    private String password;
    private String role;

}
