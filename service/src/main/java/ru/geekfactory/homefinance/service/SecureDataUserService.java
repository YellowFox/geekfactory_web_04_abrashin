package ru.geekfactory.homefinance.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.geekfactory.homefinance.service.dto.UserModelPrincipal;
import ru.geekfactory.homefinance.service.dto.UserServiceDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class SecureDataUserService {

    public Authentication getAuthentication() {

        return SecurityContextHolder.getContext().getAuthentication();
    }

    public Long getUserId() {
        UserModelPrincipal userModel = (UserModelPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userModel.getUserId();
    }

    public String getUserRole() {
        UserModelPrincipal userModel = (UserModelPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userModel.getUserRole();
    }

    public void updateUserInContextSecureSpring(UserServiceDto userServiceDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserModelPrincipal userModel = (UserModelPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userModel.setPassword(userServiceDto.getPassword());
        userModel.setLogin(userServiceDto.getLogin());
        List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());
        updatedAuthorities.add(new SimpleGrantedAuthority(userServiceDto.getRole()));
        Authentication newAuth = new UsernamePasswordAuthenticationToken(userModel, auth.getCredentials(), updatedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }
}
