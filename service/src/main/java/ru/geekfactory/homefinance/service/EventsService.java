package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.dao.model.EventsModel;
import ru.geekfactory.homefinance.dao.repository.EventsRepository;
import ru.geekfactory.homefinance.service.converters.AccountConverter;
import ru.geekfactory.homefinance.service.converters.CategoriesConverter;
import ru.geekfactory.homefinance.service.converters.EventsConverter;
import ru.geekfactory.homefinance.service.dto.EventsServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@org.springframework.stereotype.Service
public class EventsService {

    private final EventsRepository eventsRepository;
    private final EventsConverter eventsConverter;
    private final CategoriesConverter categoriesConverter;
    private final AccountConverter accountConverter;
    private final OperationsService operationsService;

    @Autowired
    public EventsService(EventsRepository eventsRepository, EventsConverter eventsConverter, CategoriesConverter categoriesConverter, AccountConverter accountConverter, OperationsService operationsService) {
        this.eventsRepository = eventsRepository;
        this.eventsConverter = eventsConverter;
        this.categoriesConverter = categoriesConverter;
        this.accountConverter = accountConverter;
        this.operationsService = operationsService;
    }

    public Optional<EventsServiceDto> findById(Long id) {
        Optional<EventsModel> eventsModel = eventsRepository.findById(id);
        return eventsConverter.repEventToServiceEventOpt(eventsModel);
    }

    public Collection<EventsServiceDto> findAll() {
        LocalDateTime timeNow = LocalDateTime.now().withNano(0).withSecond(0);
        Collection<EventsModel> eventsModels = eventsRepository.findAll();
        for (EventsModel event : eventsModels) {
            if (timeNow.isAfter(event.getDate()) & event.getIsActive()) {
                OperationServiceDto operationServiceDto = new OperationServiceDto();
                operationServiceDto.setDescription(event.getName());
                operationServiceDto.setCategories(categoriesConverter.repCatCollToServiceCatColl(event.getCategories()));
                operationServiceDto.setAmount(event.getAmount());
                operationServiceDto.setAccountModel(accountConverter.repAccountToServiceAccount(event.getAccountModel()));
                operationServiceDto.setDate(event.getDate());
                event.setIsActive(false);
                update(eventsConverter.repEventToServiceEvent(event));
                operationsService.save(operationServiceDto);
            }
        }
        return eventsConverter.repEventCollToServiceEventColl(eventsModels);
    }

    public boolean remove(Long id) {
        eventsRepository.deleteById(id);
        return false;
    }

    public EventsServiceDto save(EventsServiceDto model) {
        return eventsConverter.repEventToServiceEvent(eventsRepository.save(eventsConverter.serviceEventToEventRep(model)));
    }

    public boolean update(EventsServiceDto model) {
        return eventsRepository.save(eventsConverter.serviceEventToEventRep(model)) != null;
    }
}
