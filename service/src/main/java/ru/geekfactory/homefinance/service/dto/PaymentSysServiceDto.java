package ru.geekfactory.homefinance.service.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaymentSysServiceDto {
    private Long id;
    private String name;
}
