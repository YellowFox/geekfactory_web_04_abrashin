CREATE TABLE Bank_tbl
(
    id_bank INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name    VARCHAR(20)
);

CREATE TABLE PaymentSys_tbl
(
    id_paymentSystem INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name    VARCHAR(20)
);

CREATE TABLE Currency_tbl
(
    id_currency INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(20),
    symbol      CHAR(1),
    rate        DOUBLE
);

CREATE TABLE Categories_tbl
(
    id_categories INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    parent_id     INT,
    name          VARCHAR(20),

    FOREIGN KEY (parent_id) REFERENCES Categories_tbl (id_categories)
);

CREATE TABLE Account_tbl
(
    id_account  INT AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(20),
    amount      DOUBLE,
    bank_id INT,
    paymentSystem_id INT,
    currency_id INT,
    accountType VARCHAR(20),
    isActiv BOOLEAN,

    FOREIGN KEY (bank_id) REFERENCES Bank_tbl (id_bank),
    FOREIGN KEY (paymentSystem_id) REFERENCES PaymentSys_tbl (id_paymentSystem),
    FOREIGN KEY (currency_id) REFERENCES Currency_tbl (id_currency)
);

CREATE TABLE Events_tbl /* те которые по расписанию*/
(
    id_events  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name       VARCHAR(20),
    date       DATETIME,
    account_id INT,
    amount DOUBLE,

    FOREIGN KEY (account_id) REFERENCES Account_tbl (id_account)
);

CREATE TABLE events_categories_tbl
(
    events_id  INTEGER NOT NULL,
    categories_id INTEGER NOT NULL,

    FOREIGN KEY (events_id) REFERENCES Events_tbl (id_events) ON update CASCADE,
    FOREIGN KEY (categories_id) REFERENCES Categories_tbl (id_categories) ON update CASCADE
);


CREATE TABLE Operation_tbl /* история*/
(
    id_operations INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_id    INT,
    description   varchar(50),
    date          DATETIME,
    amount DOUBLE,

    FOREIGN KEY (account_id) REFERENCES Account_tbl (id_account)
);

CREATE TABLE operation_categories_tbl
(
    operation_id  INTEGER NOT NULL,
    categories_id INTEGER NOT NULL,

    FOREIGN KEY (operation_id) REFERENCES Operations (id_operations) ON update CASCADE,
    FOREIGN KEY (categories_id) REFERENCES Categories_tbl (id_categories) ON update CASCADE
);


/*CREATE TABLE Budgets_tbl /* куда бы добавить бюджет, к какой таблице,
                        составная таблица из 3 fk или сделать новую
                        либо выделить operation_category в отдельную сущность */
/*(
    id_budgets    INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name          VARCHAR(20),
    categories_id INT NOT NULL,
    date          DATE,
    amount        DOUBLE,
    FOREIGN KEY (categories_id) REFERENCES Categories_tbl (id_categories)

); */