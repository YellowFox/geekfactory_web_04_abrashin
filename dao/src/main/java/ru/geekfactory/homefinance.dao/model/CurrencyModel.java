package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Currency_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_currency")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "symbol")
    private String symbol;

    public CurrencyModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CurrencyModel(Long id) {
        this.id = id;
    }

    public CurrencyModel(String name) {
        this.name = name;
    }

    public CurrencyModel(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }
}
