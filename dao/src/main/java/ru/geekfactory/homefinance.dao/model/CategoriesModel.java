package ru.geekfactory.homefinance.dao.model;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "Categories_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CategoriesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categories")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private CategoriesModel parent;

    @Column(name = "name")
    private String name;

    public CategoriesModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoriesModel(CategoriesModel parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    public CategoriesModel(String name) {
        this.name = name;
    }

    public CategoriesModel(Long id) {
        this.id = id;
    }

}
