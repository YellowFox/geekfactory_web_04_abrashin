package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "PaymentSys_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentSystemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_paymentSystem")
    private long id;
    @Column(name = "name")
    private String name;

    public PaymentSystemModel(String name){
        this.name = name;
    }

    public PaymentSystemModel(Long id){
        this.id = id;
    }

    public PaymentSystemModel(Long id, String name){
        this.id = id;
        this.name = name;
    }
}


