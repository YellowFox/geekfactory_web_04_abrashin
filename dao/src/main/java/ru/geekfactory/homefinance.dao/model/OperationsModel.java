package ru.geekfactory.homefinance.dao.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "Operation_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperationsModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_operations")
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "account_id")
    private AccountModel accountModel;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "operation_categories_tbl",
            joinColumns = {@JoinColumn(name = "operation_id")},
            inverseJoinColumns = {@JoinColumn(name = "categories_id")})
    private Collection<CategoriesModel> categories;
    @Column(name = "date")
    private LocalDateTime date;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "description")
    private String description;

    public OperationsModel(Long id, AccountModel accountModel, LocalDateTime date, BigDecimal amount, String description) {
        this.id = id;
        this.accountModel = accountModel;
        this.date = date;
        this.amount = amount;
        this.description = description;
    }
}
