package ru.geekfactory.homefinance.dao.model;

import java.math.BigDecimal;
import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "Account_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "accountType")
    private String accountType;
    @Column(name = "amount")
    private BigDecimal amount;
    @ManyToOne
    @JoinColumn(name = "bank_id")
    private BankModel bankModel;
    @ManyToOne
    @JoinColumn(name = "paymentSystem_id")
    private PaymentSystemModel paySystemModel;
    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyModel currencyModel;
    @Column(name = "isActiv")
    private boolean isActive;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel userModel;


    public AccountModel(String name) {
        this.name = name;
    }

    public AccountModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public AccountModel(Long  id, String name, BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
    }
}
