package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "Bank_tbl")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BankModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_bank")
    private Long id;
    @Column(name = "name")
    private String name;

    public BankModel(String name) {
        this.name = name;
    }

    public BankModel(Long id) {
        this.id = id;
    }

}
