package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "Events_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EventsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_events")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "date")
    private LocalDateTime date;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private AccountModel accountModel;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "active")
    private Boolean isActive;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "events_categories_tbl",
            joinColumns = {@JoinColumn(name = "events_id")},
            inverseJoinColumns = {@JoinColumn(name = "categories_id")})
    private Collection<CategoriesModel> categories;

    public EventsModel(Long id) {
        this.id = id;
    }

    public EventsModel(long id, String name, LocalDateTime date, AccountModel accountModel, BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.accountModel = accountModel;
        this.amount = amount;
    }
}
