package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.*;

import java.util.Collection;

@Repository
public interface OperationsRepository extends JpaRepository<OperationsModel, Long> {
    Collection<OperationsModel> findAllByAccountModelIsIn (Collection<AccountModel> accountModels);

}
