package ru.geekfactory.homefinance.dao.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class CurrencyRepository{

    @PersistenceContext
    private EntityManager em;

    public Optional<CurrencyModel> findByName(String name) {
        return Optional.empty();
    }

    public Optional<CurrencyModel> findById(Long id) {
        return Optional.ofNullable(em.find(CurrencyModel.class, id));
    }

    public Collection<CurrencyModel> findAll() {
        return (Collection<CurrencyModel>)em.createQuery("SELECT a FROM CurrencyModel a").getResultList();
    }

    public boolean delRefInDependentEntities(Long id) {
        int result = 0;
        Query querySelect = em.createQuery("SELECT acc from AccountModel acc where currency_id = :id");
        querySelect.setParameter("id", id);
        List<AccountModel> list = querySelect.getResultList();
        for (AccountModel acc : list) {
            Query queryUpdate = em.createQuery("update AccountModel set" +
                    " currency_id = :currency_id" +
                    " where id = :idAcc"
            );
            queryUpdate.setParameter("currency_id", null);
            queryUpdate.setParameter("idAcc", acc.getId());
            result = queryUpdate.executeUpdate();
        }
        return result != 0;
    }

    public boolean remove(Long id) {
        delRefInDependentEntities(id);
        em.remove(em.find(CurrencyModel.class, id));
        return true;
    }

    public CurrencyModel save(CurrencyModel model) {
        em.persist(model);
        return model;
    }

    public boolean update(CurrencyModel model) {
        if (Objects.nonNull(em.merge(model))){
            return true;
        }else {
            return false;
        }
    }
}