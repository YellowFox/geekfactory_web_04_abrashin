package ru.geekfactory.homefinance.dao.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.BankModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class BankRepository {

    @PersistenceContext
    private EntityManager em;

    public Optional<BankModel> findByName(String name) {
        return Optional.empty();
    }

    public Optional<BankModel> findById(Long id) {
        return Optional.ofNullable(em.find(BankModel.class, id));
    }

    public Collection<BankModel> findAll() {
        return (Collection<BankModel>) em.createQuery("SELECT a FROM BankModel a").getResultList();
    }

    public boolean delRefInDependentEntities(Long id) {
        int result = 0;
        Query querySelect = em.createQuery("SELECT acc from AccountModel acc where bank_id = :id");
        querySelect.setParameter("id", id);
        List<AccountModel> list = querySelect.getResultList();
        for (AccountModel acc : list) {
            Query queryUpdate = em.createQuery("update AccountModel set" +
                    " bank_id = :bank_id" +
                    " where id = :idAcc"
            );
            queryUpdate.setParameter("bank_id", null);
            queryUpdate.setParameter("idAcc", acc.getId());
            result = queryUpdate.executeUpdate();
        }
        return result != 0;
    }

    public boolean remove(Long id) {
        delRefInDependentEntities(id);
        em.remove(em.find(BankModel.class, id));
        return true;
    }

    public BankModel save(BankModel model) {
        em.persist(model);
        return model;
    }

    public boolean update(BankModel model) {
        return em.merge(model) != null;
    }

}