package ru.geekfactory.homefinance.dao.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.EventsModel;
import ru.geekfactory.homefinance.dao.model.OperationsModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class AccountRepository {

    @PersistenceContext
    private EntityManager em;

    public Optional<AccountModel> findByName(String name) {
        return Optional.empty();
    }

    public Optional<AccountModel> findById(Long id) {
        return Optional.ofNullable(em.find(AccountModel.class, id));
    }

    public Collection<AccountModel> findAll() {
        return (Collection<AccountModel>)em.createQuery("SELECT a FROM AccountModel a").getResultList();
    }

    public Collection<AccountModel> findAllAccountsWithId(Long id) {
        Query querySelect = em.createQuery("SELECT a FROM AccountModel a where user_id = :id");
        querySelect.setParameter("id", id);
        return (Collection<AccountModel>) querySelect.getResultList();
    }

    public boolean delRefInDependentEntities(Long id) {
        int result = 0;
        Query querySelect = em.createQuery("SELECT acc from OperationsModel acc where account_id = :id");
        querySelect.setParameter("id", id);
        List<OperationsModel> listAcc = querySelect.getResultList();
        for (OperationsModel acc : listAcc) {
            Query queryUpdate = em.createQuery("delete OperationsModel" +
                    " where id = :idAcc"
            );
            queryUpdate.setParameter("idAcc", acc.getId());
            result = queryUpdate.executeUpdate();
        }

        querySelect = em.createQuery("SELECT event from EventsModel event where account_id = :id");
        querySelect.setParameter("id", id);
        List<EventsModel> listEvent = querySelect.getResultList();
        for (EventsModel event : listEvent) {
            Query queryUpdate = em.createQuery("delete EventsModel" +
                    " where id = :idAcc"
            );
            queryUpdate.setParameter("idAcc", event.getId());
            result = queryUpdate.executeUpdate();
        }
        return result != 0;
    }

    public boolean remove(Long id) {
        delRefInDependentEntities(id);
        em.remove(em.find(AccountModel.class, id));
        return true;
    }

    public AccountModel save(AccountModel model) {
        em.persist(model);
        return model;
    }

    public boolean update(AccountModel model) {
        return Objects.nonNull(em.merge(model));
    }
}