package ru.geekfactory.homefinance.dao.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.PaymentSystemModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PreRemove;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class PaymentSysRepository {

    @PersistenceContext
    private EntityManager em;

    public Optional<PaymentSystemModel> findByName(String name) {
        return Optional.empty();
    }

    public Optional<PaymentSystemModel> findById(Long id) {
        return Optional.ofNullable(em.find(PaymentSystemModel.class, id));
    }

    public Collection<PaymentSystemModel> findAll() {
        return (Collection<PaymentSystemModel>)em.createQuery("SELECT a FROM PaymentSystemModel a").getResultList();
    }

    @PreRemove
    public boolean delRefInDependentEntities(Long id) {
        int result = 0;
        Query querySelect = em.createQuery("SELECT acc from AccountModel acc where paymentSystem_id = :id");
        querySelect.setParameter("id", id);
        List<AccountModel> list = querySelect.getResultList();
        for (AccountModel acc : list) {
            Query queryUpdate = em.createQuery("update AccountModel set" +
                    " paymentSystem_id = :paymentSystem_id" +
                    " where id = :idAcc"
            );
            queryUpdate.setParameter("paymentSystem_id", null);
            queryUpdate.setParameter("idAcc", acc.getId());
            result = queryUpdate.executeUpdate();
        }
        return result != 0;
    }

    public boolean remove(Long id) {
        delRefInDependentEntities(id);
        em.remove(em.find(PaymentSystemModel.class, id));
        return true;
    }

    public PaymentSystemModel save(PaymentSystemModel model) {
        em.persist(model);
        return model;
    }

    public boolean update(PaymentSystemModel model) {
        if (Objects.nonNull(em.merge(model))){
            return true;
        }else {
            return false;
        }
    }
}