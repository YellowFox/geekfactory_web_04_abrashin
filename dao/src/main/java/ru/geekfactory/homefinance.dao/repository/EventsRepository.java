package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.EventsModel;

@Repository
public interface EventsRepository extends JpaRepository<EventsModel, Long> {

}
