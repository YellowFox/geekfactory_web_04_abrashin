package ru.geekfactory.homefinance.dao.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import ru.geekfactory.homefinance.dao.model.CategoriesModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class CategoriesRepository {

    @PersistenceContext
    private EntityManager em;

    public Optional<CategoriesModel> findByName(String name) {
        return Optional.empty();
    }

    public Optional<CategoriesModel> findById(Long id) {
        return Optional.ofNullable(em.find(CategoriesModel.class, id));
    }

    public Collection<CategoriesModel> findAll() {
        return (Collection<CategoriesModel>)em.createQuery("SELECT a FROM CategoriesModel a").getResultList();
    }

    public boolean remove(Long id) {
        em.remove(em.find(CategoriesModel.class, id));
        return true;
    }

    public CategoriesModel save(CategoriesModel model) {
        em.persist(model);
        return model;
    }

    public boolean update(CategoriesModel model) {
        if (Objects.nonNull(em.merge(model))){
            return true;
        }else {
            return false;
        }
    }
}