package ru.geekfactory.homefinance.dao.exceptions;

public class DbFilesConfigExistException extends RuntimeException {

    public DbFilesConfigExistException(String message) {
        super(message);
    }

    public DbFilesConfigExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbFilesConfigExistException(Throwable cause) {
        super(cause);
    }

}
