package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.*;
import ru.geekfactory.homefinance.service.dto.*;
import ru.geekfactory.homefinance.web.adapter.AccountDto;
import ru.geekfactory.homefinance.web.dto.AccountWebDto;
import java.util.Collection;

@Controller
@RequestMapping("/user_page")
public class UsersController {

    private final AccountService accountService;
    private final BankService bankService;
    private final PaymentSysService paymentSystemService;
    private final CurrencyService currencyService;
    private UserService userService;
    private SecureDataUserService secureDataUserService;

    @Autowired
    public UsersController(AccountService accountService, BankService bankService,
                           PaymentSysService paymentSystemService, CurrencyService currencyService,
                           UserService userService, SecureDataUserService secureDataUserService) {

        this.accountService = accountService;
        this.bankService = bankService;
        this.paymentSystemService = paymentSystemService;
        this.currencyService = currencyService;
        this.userService = userService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String userPage(Model model) {
        Long id = secureDataUserService.getUserId();
        UserServiceDto userServiceDto = userService.findById(id).get();
        Collection<UserServiceDto> userServiceDtoCollection = userService.findAll();
        Collection<AccountServiceDto> allAccounts = accountService.findAllAccountsWithId(id);
        Collection<BankServiceDto> allBanks = bankService.findAll();
        Collection<PaymentSysServiceDto> allPaymentSystems = paymentSystemService.findAll();
        Collection<CurrencyServiceDto> allCurrencies = currencyService.findAll();

        model.addAttribute("accounts", allAccounts);
        model.addAttribute("banks", allBanks);
        model.addAttribute("paymentSystems", allPaymentSystems);
        model.addAttribute("currencies", allCurrencies);
        model.addAttribute("user", userServiceDto.getLogin());
        model.addAttribute("allUsers", userServiceDtoCollection);
        model.addAttribute("currentlyUser", userServiceDto);
        return "users/userPage/user_page";
    }

    @PutMapping(value = "/edit_password", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody UserServiceDto userServiceDto) {
        userService.update(userServiceDto);
        secureDataUserService.updateUserInContextSecureSpring(userServiceDto);
    }

    @PutMapping(value = "/edit_account", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateAcc(@RequestBody AccountWebDto accountWebDto) {
        AccountDto accountDto = new AccountDto(bankService, paymentSystemService, currencyService, userService);
        accountService.update(accountDto.AccountWebToAccountService(accountWebDto));
    }

    @PostMapping(value = "/save_account", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveAcc(@RequestBody AccountWebDto accountWebDto) {
        AccountDto accountDto = new AccountDto(bankService, paymentSystemService, currencyService, userService);
        accountService.save(accountDto.AccountWebToAccountService(accountWebDto));
    }

    @DeleteMapping(value = "/del")
    public String remove(@RequestBody AccountServiceDto accountModel) {
        accountService.remove(accountModel.getId());
        return "account/account_list";
    }
}
