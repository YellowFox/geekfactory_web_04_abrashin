package ru.geekfactory.homefinance.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.CurrencyService;
import ru.geekfactory.homefinance.service.SecureDataUserService;
import ru.geekfactory.homefinance.service.dto.CurrencyServiceDto;
import java.util.Collection;

@Controller
@RequestMapping("/currency")
public class CurrencyController {

    private final CurrencyService currencyService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public CurrencyController(CurrencyService currencyService,
                              SecureDataUserService secureDataUserService) {
        this.currencyService = currencyService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        Collection<CurrencyServiceDto> all = currencyService.findAll();
        model.addAttribute("currencies", all);
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());
        return "currency/currency_list";
    }

    @GetMapping("/{id}")
    public
    String findById(@PathVariable long id, Model model) {
        model.addAttribute("currencies", currencyService.findById(id).get());
        return "currency/currency_list";
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    String save(@RequestBody CurrencyServiceDto currencyServiceDto) {
        currencyService.save(currencyServiceDto);
        return "currency/currency_list";
    }

    @PutMapping(value = "/edit")
    public
    String update(@RequestBody CurrencyServiceDto currencyServiceDto) {
        currencyService.update(currencyServiceDto);
        return "currency/currency_list";
    }

    @DeleteMapping(value = "/del")
    public
    String remove(@RequestBody CurrencyServiceDto currencyServiceDto) {
        currencyService.remove(currencyServiceDto.getId());
        return "currency/currency_list";
    }
}
