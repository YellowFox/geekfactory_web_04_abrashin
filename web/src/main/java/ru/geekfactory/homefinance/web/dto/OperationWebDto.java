package ru.geekfactory.homefinance.web.dto;

import lombok.*;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;

import java.math.BigDecimal;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OperationWebDto {
    private Long id;
    private String description;
    private Long account;
    private BigDecimal amount;
    private String date;
    private Collection<Long> categories;
}
