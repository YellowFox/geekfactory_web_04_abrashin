package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.BankService;
import ru.geekfactory.homefinance.service.SecureDataUserService;
import ru.geekfactory.homefinance.service.dto.BankServiceDto;

import java.util.Collection;

@Controller
@RequestMapping("/bank")
public class BankController {

    private final BankService bankService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public BankController(BankService bankService, SecureDataUserService secureDataUserService) {
        this.bankService = bankService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        Collection<BankServiceDto> all = bankService.findAll();
        model.addAttribute("banks", all);
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());
        return "bank/bank_list";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable long id, Model model) {
        model.addAttribute("banks", bankService.findById(id).get());
        return "bank/bank_list";
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public String save(@RequestBody BankServiceDto bankServiceDto) {
        bankService.save(bankServiceDto);
        return "bank/bank_list";
    }

    @PutMapping(value = "/edit")
    public String update(@RequestBody BankServiceDto bankServiceDto) {
        bankService.update(bankServiceDto);
        return "bank/bank_list";
    }

    @DeleteMapping(value = "/del")
    public String remove(@RequestBody BankServiceDto bankServiceDto) {
        bankService.remove(bankServiceDto.getId());
        return "bank/bank_list";
    }
}
