package ru.geekfactory.homefinance.web.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.service.AccountService;
import ru.geekfactory.homefinance.service.CategoriesService;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;
import ru.geekfactory.homefinance.web.dto.OperationWebDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class OperationDto {

    private CategoriesService categoriesService;
    private AccountService accountService;

    @Autowired
    public OperationDto(CategoriesService catServiceDtoService,
                        AccountService accountService) {

        this.accountService = accountService;
        this.categoriesService = catServiceDtoService;
    }

    public OperationServiceDto OperationsWebToOperationsService(OperationWebDto operationWebDto) {
        OperationServiceDto operationServiceDto = new OperationServiceDto();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(operationWebDto.getDate(), formatter);
        if (operationWebDto.getId() != null) {
            operationServiceDto.setId(operationWebDto.getId());
        } else {
            operationServiceDto.setId(null);
        }
        operationServiceDto.setDescription(operationWebDto.getDescription());
        operationServiceDto.setAmount(operationWebDto.getAmount());
        operationServiceDto.setDate(dateTime);
        if (operationWebDto.getAccount() != null) {
            operationServiceDto.setAccountModel(accountService.findById(operationWebDto.getAccount()).orElse(null));
        } else {
            operationServiceDto.setAccountModel(null);
        }
        if (operationWebDto.getCategories() != null && !operationWebDto.getCategories().isEmpty()) {
            List<CategoriesServiceDto> categoriesModels = new ArrayList<>();
            for (Long cat : operationWebDto.getCategories()) {
                if (cat != null) {
                    categoriesModels.add(categoriesService.findById(cat).orElse(null));
                } else {
                    operationServiceDto.setCategories(null);
                    return operationServiceDto;
                }
            }
            operationServiceDto.setCategories(categoriesModels);
        } else {
            operationServiceDto.setCategories(null);
        }
        return operationServiceDto;
    }
}
