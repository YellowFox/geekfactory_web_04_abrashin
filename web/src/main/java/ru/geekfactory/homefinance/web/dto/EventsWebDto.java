package ru.geekfactory.homefinance.web.dto;

import lombok.*;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;

import java.math.BigDecimal;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventsWebDto {

    private Long id;
    private String name;
    private String date;
    private Long account;
    private String isActive;
    private Collection<Long> categories;
    private BigDecimal amount;
}