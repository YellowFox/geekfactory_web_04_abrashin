package ru.geekfactory.homefinance.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.CategoriesService;
import ru.geekfactory.homefinance.service.SecureDataUserService;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.web.adapter.CategoriesDto;
import ru.geekfactory.homefinance.web.dto.CategoriesWebDto;

import java.util.Collection;

@Controller
@RequestMapping("/categories")
public class CategoriesController {

    private CategoriesService categoriesService;
    private SecureDataUserService secureDataUserService;

    @Autowired
    public CategoriesController(CategoriesService categoriesService,
                                SecureDataUserService secureDataUserService) {
        this.categoriesService = categoriesService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        Collection<CategoriesServiceDto> all = categoriesService.findAll();
        model.addAttribute("categories", all);
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());
        return "categories/categories_list";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable long id, Model model) {
        model.addAttribute("categories", categoriesService.findById(id).get());
        return "categories/categories_list";
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String save(@RequestBody CategoriesWebDto CategoriesWebDto) {
        CategoriesDto categoriesModel = new CategoriesDto(categoriesService);
        categoriesService.save(categoriesModel.CategoriesWebToServiceCategories(CategoriesWebDto));
        return "categories/categories_list";
    }

    @PutMapping(value = "/edit")
    public String update(@RequestBody CategoriesWebDto CategoriesWebDto) {
        CategoriesDto categoriesModel = new CategoriesDto(categoriesService);
        categoriesService.update(categoriesModel.CategoriesWebToServiceCategories(CategoriesWebDto));
        return "categories/categories_list";
    }

    @DeleteMapping(value = "/del")
    public String remove(@RequestBody CategoriesServiceDto categoriesModel) {
        categoriesService.remove(categoriesModel.getId());
        return "categories/categories_list";
    }
}
