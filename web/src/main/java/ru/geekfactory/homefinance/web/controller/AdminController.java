package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.SecureDataUserService;
import ru.geekfactory.homefinance.service.UserService;
import ru.geekfactory.homefinance.service.dto.UserServiceDto;

@Controller
@RequestMapping("/admin_page")
public class AdminController {

    private final UserService userService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public AdminController(UserService userService, SecureDataUserService secureDataUserService) {
        this.userService = userService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String adminPage(Model model) {
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());
        model.addAttribute("allUsers", userService.findAll());
        return "users/adminPage/admin_page";
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    String save(@RequestBody UserServiceDto userServiceDto) {
        userService.save(userServiceDto);
        return "users/adminPage/admin_page";
    }

    @PutMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    String update(@RequestBody UserServiceDto userServiceDto) {
        userService.update(userServiceDto);
        return "users/adminPage/admin_page";
    }

    @DeleteMapping(value = "/del", consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    String remove(@RequestBody UserServiceDto userServiceDto) {
        userService.remove(userServiceDto.getId());
        return "users/adminPage/admin_page";
    }

}
