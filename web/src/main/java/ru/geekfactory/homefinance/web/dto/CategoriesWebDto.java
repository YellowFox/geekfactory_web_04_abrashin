package ru.geekfactory.homefinance.web.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoriesWebDto {
    private Long id;
    private Long parentId;
    private String name;
}
