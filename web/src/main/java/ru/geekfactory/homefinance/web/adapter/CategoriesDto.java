package ru.geekfactory.homefinance.web.adapter;

import ru.geekfactory.homefinance.service.CategoriesService;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.web.dto.CategoriesWebDto;

public class CategoriesDto {

    private CategoriesService categoriesService;

    public CategoriesDto(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    public CategoriesServiceDto CategoriesWebToServiceCategories(CategoriesWebDto categoriesWebDto) {
        CategoriesServiceDto categoriesServiceDto = new CategoriesServiceDto();
        categoriesServiceDto.setId(categoriesWebDto.getId());
        categoriesServiceDto.setName(categoriesWebDto.getName());
        if (categoriesWebDto.getParentId() != null) {
            categoriesServiceDto.setParent(categoriesService.findById(categoriesWebDto.getParentId()).get());
        } else {
            categoriesServiceDto.setParent(null);
        }
        return categoriesServiceDto;
    }
}
