package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.*;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.OperationServiceDto;
import ru.geekfactory.homefinance.web.adapter.OperationDto;
import ru.geekfactory.homefinance.web.dto.OperationWebDto;

import java.util.Collection;

@Controller
@RequestMapping("/operations")
public class OperationController {
    private final OperationsService operationsService;
    private final CategoriesService categoriesService;
    private final AccountService accountService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public OperationController(OperationsService operationsService,
                               CategoriesService catServiceDtoService,
                               AccountService accountService,
                               SecureDataUserService secureDataUserService) {
        this.operationsService = operationsService;
        this.accountService = accountService;
        this.categoriesService = catServiceDtoService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        String role = secureDataUserService.getUserRole();
        if (role.equalsIgnoreCase("USER")) {
            Long id = secureDataUserService.getUserId();
            Collection<AccountServiceDto> allAccountsWithIdUser = accountService.findAllAccountsWithId(id);
            Collection<OperationServiceDto> allOperation = operationsService.findByAccountModelById(allAccountsWithIdUser);
            Collection<CategoriesServiceDto> allCategories = categoriesService.findAll();
            model.addAttribute("operations", allOperation);
            model.addAttribute("accounts", allAccountsWithIdUser);
            model.addAttribute("categories", allCategories);
            model.addAttribute("user", secureDataUserService.getAuthentication().getName());
            return "operations/operations_list";
        }else if (role.equalsIgnoreCase("ADMIN")){
            Long id = secureDataUserService.getUserId();
            Collection<AccountServiceDto> allAccounts = accountService.findAll();
            Collection<OperationServiceDto> allOperation = operationsService.findAll();
            Collection<CategoriesServiceDto> allCategories = categoriesService.findAll();
            model.addAttribute("operations", allOperation);
            model.addAttribute("accounts", allAccounts);
            model.addAttribute("categories", allCategories);
            model.addAttribute("user", secureDataUserService.getAuthentication().getName());
            return "operations/operations_list";
        }else {
            return "operations/operations_list";
        }
    }

    @GetMapping(value = "/{operationId}")
    public String getTestData(@PathVariable Integer operationId, Model model) {
        OperationServiceDto operation = operationsService.findById(Long.valueOf(operationId)).get();
        model.addAttribute("operations", operation);
        return "operations/operations_list";
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public
    String save(@RequestBody OperationWebDto operationWebDto) {
        OperationDto operationDto = new OperationDto(categoriesService, accountService);
        operationsService.save(operationDto.OperationsWebToOperationsService(operationWebDto));
        return "operations/operations_list";
    }

    @PutMapping(value = "/edit")
    public
    String edit(@RequestBody OperationWebDto operationWebDto) {
        OperationDto operationDto = new OperationDto(categoriesService, accountService);
        operationsService.update(operationDto.OperationsWebToOperationsService(operationWebDto));
        return "operations/operations_list";
    }

    @DeleteMapping(value = "/del")
    public
    String remove(@RequestBody OperationServiceDto operationServiceDto) {
        operationsService.remove(operationServiceDto.getId());
        return "operations/operations_list";
    }

}
