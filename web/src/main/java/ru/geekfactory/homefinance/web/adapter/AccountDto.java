package ru.geekfactory.homefinance.web.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.service.*;
import ru.geekfactory.homefinance.service.dto.*;
import ru.geekfactory.homefinance.web.dto.AccountWebDto;

public class AccountDto {

    private BankService bankService;
    private PaymentSysService paymentSystemService;
    private CurrencyService currencyService;
    private UserService userService;

    @Autowired
    public AccountDto(BankService bankService, PaymentSysService paymentSystemService,
                      CurrencyService currencyService, UserService userService) {
        this.bankService = bankService;
        this.paymentSystemService = paymentSystemService;
        this.currencyService = currencyService;
        this.userService = userService;
    }

    public AccountServiceDto AccountWebToAccountService(AccountWebDto accountWebDto) {
        AccountServiceDto accountServiceDto = new AccountServiceDto();
        if (accountWebDto.getId() != null) {
            accountServiceDto.setId(accountWebDto.getId());
        } else {
            accountServiceDto.setId(null);
        }
        accountServiceDto.setName(accountWebDto.getName());
        accountServiceDto.setAccountType(accountWebDto.getAccountType());
        accountServiceDto.setAmount(accountWebDto.getAmount());
        if (accountWebDto.getIsActive().equals("1")) {
            accountServiceDto.setActive(true);
        } else {
            accountServiceDto.setActive(false);
        }
        if (accountWebDto.getBank() != null) {
            accountServiceDto.setBankModel(bankService.findById(accountWebDto.getBank()).orElse(null));
        } else {
            accountServiceDto.setBankModel(null);
        }
        if (accountWebDto.getCurrency() != null) {
            accountServiceDto.setCurrencyModel(currencyService.findById(accountWebDto.getCurrency()).orElse(null));
        } else {
            accountServiceDto.setCurrencyModel(null);
        }
        if (accountWebDto.getPaymentSystem() != null) {
            accountServiceDto.setPaySystemModel(paymentSystemService.findById(accountWebDto.getPaymentSystem()).orElse(null));
        } else {
            accountServiceDto.setPaySystemModel(null);
        }
        if (accountWebDto.getUser() != null) {
            accountServiceDto.setUserModel(userService.findById(accountWebDto.getUser()).orElse(null));
        } else {
            accountServiceDto.setUserModel(null);
        }
        return accountServiceDto;
    }
}
