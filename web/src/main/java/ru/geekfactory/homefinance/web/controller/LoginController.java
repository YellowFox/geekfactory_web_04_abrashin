package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.UserService;
import ru.geekfactory.homefinance.service.dto.UserServiceDto;

@Controller
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/login")
    public String login() {

        return "login/login";
    }

    @GetMapping("/register")
    public String register() {

        return "login/register";
    }

    @PostMapping(value = "/register/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String edit(@RequestBody UserServiceDto userServiceDto) {
        userService.save(userServiceDto);
        return "login/login";
    }
}
