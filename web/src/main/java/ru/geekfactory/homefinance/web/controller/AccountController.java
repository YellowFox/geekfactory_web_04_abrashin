package ru.geekfactory.homefinance.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.*;
import ru.geekfactory.homefinance.service.dto.*;
import ru.geekfactory.homefinance.web.adapter.AccountDto;
import ru.geekfactory.homefinance.web.dto.AccountWebDto;

import java.util.Collection;

@Controller
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;
    private final BankService bankService;
    private final PaymentSysService paymentSystemService;
    private final CurrencyService currencyService;
    private final UserService userService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public AccountController(AccountService accountService, BankService bankService,
                             PaymentSysService paymentSystemService, CurrencyService currencyService,
                             UserService userService, SecureDataUserService secureDataUserService) {
        this.accountService = accountService;
        this.bankService = bankService;
        this.paymentSystemService = paymentSystemService;
        this.currencyService = currencyService;
        this.userService = userService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        String role = secureDataUserService.getUserRole();
        if (role.equalsIgnoreCase("USER")) {
            Long id = secureDataUserService.getUserId();
            Collection<AccountServiceDto> allAccounts = accountService.findAllAccountsWithId(id);
            Collection<BankServiceDto> allBanks = bankService.findAll();
            Collection<PaymentSysServiceDto> allPaymentSystems = paymentSystemService.findAll();
            Collection<CurrencyServiceDto> allCurrencies = currencyService.findAll();
            Collection<UserServiceDto> allUsers = userService.findAll();
            model.addAttribute("accounts", allAccounts);
            model.addAttribute("banks", allBanks);
            model.addAttribute("paymentSystems", allPaymentSystems);
            model.addAttribute("currencies", allCurrencies);
            model.addAttribute("users", allUsers);
            model.addAttribute("user", secureDataUserService.getAuthentication().getName());
            return "account/account_list";
        } else if (role.equalsIgnoreCase("ADMIN")) {
            Long id = secureDataUserService.getUserId();
            Collection<AccountServiceDto> allAccounts = accountService.findAll();
            Collection<BankServiceDto> allBanks = bankService.findAll();
            Collection<PaymentSysServiceDto> allPaymentSystems = paymentSystemService.findAll();
            Collection<CurrencyServiceDto> allCurrencies = currencyService.findAll();
            Collection<UserServiceDto> allUsers = userService.findAll();
            model.addAttribute("accounts", allAccounts);
            model.addAttribute("banks", allBanks);
            model.addAttribute("paymentSystems", allPaymentSystems);
            model.addAttribute("currencies", allCurrencies);
            model.addAttribute("users", allUsers);
            model.addAttribute("user", secureDataUserService.getAuthentication().getName());
            return "account/account_list";
        } else {
            return "account/account_list";
        }
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable long id, Model model) {
        AccountServiceDto accountServiceDto = accountService.findById(id).get();
        model.addAttribute("accounts", accountServiceDto);
        return "account/account_list";
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public String save(@RequestBody AccountWebDto accountWebDto) {
        accountWebDto.setId(null);
        AccountDto accountDto = new AccountDto(bankService, paymentSystemService, currencyService, userService);
        accountService.save(accountDto.AccountWebToAccountService(accountWebDto));
        return "account/account_list";
    }

    @PutMapping(value = "/edit")
    public String update(@RequestBody AccountWebDto accountWebDto) {
        AccountDto accountDto = new AccountDto(bankService, paymentSystemService, currencyService, userService);
        accountService.update(accountDto.AccountWebToAccountService(accountWebDto));
        return "account/account_list";
    }

    @DeleteMapping(value = "/del")
    public String remove(@RequestBody AccountServiceDto accountModel) {
        accountService.remove(accountModel.getId());
        return "account/account_list";
    }
}
