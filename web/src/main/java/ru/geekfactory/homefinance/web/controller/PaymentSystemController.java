package ru.geekfactory.homefinance.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.PaymentSysService;
import ru.geekfactory.homefinance.service.SecureDataUserService;
import ru.geekfactory.homefinance.service.dto.PaymentSysServiceDto;
import java.util.Collection;

@Controller
@RequestMapping("/payment")
public class PaymentSystemController {

    private PaymentSysService paymentSystemService;
    private SecureDataUserService secureDataUserService;

    @Autowired
    public PaymentSystemController(PaymentSysService paymentSystemService,
                                   SecureDataUserService secureDataUserService) {
        this.paymentSystemService = paymentSystemService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        Collection<PaymentSysServiceDto> all = paymentSystemService.findAll();
        model.addAttribute("paymentSystems", all);
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());

        return "paymentSystem/paymentSystem_list";
    }

    @GetMapping("/{id}")
    public
    String findById(@PathVariable long id, Model model) {
        PaymentSysServiceDto paymentSysServiceDto = paymentSystemService.findById(id).get();
        model.addAttribute("paymentSystems", paymentSysServiceDto);
        return "paymentSystem/paymentSystem_list";
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    String save(@RequestBody PaymentSysServiceDto paymentSysServiceDto) {
        paymentSystemService.save(paymentSysServiceDto);
        return "paymentSystem/paymentSystem_list";
    }

    @PutMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Boolean update(@RequestBody PaymentSysServiceDto paymentSysServiceDto) {
        return paymentSystemService.update(paymentSysServiceDto);
    }

    @DeleteMapping(value = "/del", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Boolean remove(@RequestBody PaymentSysServiceDto paymentSysServiceDto) {
        return paymentSystemService.remove(paymentSysServiceDto.getId());
    }
}
