package ru.geekfactory.homefinance.web.controller;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.geekfactory.homefinance.dao.exceptions.DbFilesConfigExistException;
import ru.geekfactory.homefinance.dao.exceptions.NotEnoughMoneyException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({SQLException.class, DataAccessException.class, DataIntegrityViolationException.class,
            DbFilesConfigExistException.class})
    public ModelAndView handleIOException(){
        ModelAndView model = new ModelAndView();
        model.setViewName("errors/dbErrors");
        return model;
    }

    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ModelAndView allExceptions(){
        ModelAndView model = new ModelAndView();
        model.setViewName("errors/allExceptions");
        return model;
    }

    @ExceptionHandler(NotEnoughMoneyException.class)
    public ModelAndView handleNotEnoughMoneyException(){
        ModelAndView model = new ModelAndView();
        model.setViewName("errors/noEnoughMoney");
        return model;
    }
}
