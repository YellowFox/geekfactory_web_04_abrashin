package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.geekfactory.homefinance.service.SecureDataUserService;

@Controller
@RequestMapping("/")
public class DefaultController {

    @Autowired
    private SecureDataUserService secureDataUserService;

    @GetMapping("/")
    public String welcome(Model model) {
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());

        return "index";
    }
}
