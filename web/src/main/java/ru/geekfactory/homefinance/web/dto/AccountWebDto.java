package ru.geekfactory.homefinance.web.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AccountWebDto {

    private Long id;
    private String name;
    private BigDecimal amount;
    private Long bank;
    private Long paymentSystem;
    private Long currency;
    private String accountType;
    private String isActive;
    private Long user;
}
