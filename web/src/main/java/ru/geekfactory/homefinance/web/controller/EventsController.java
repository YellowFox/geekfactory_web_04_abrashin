package ru.geekfactory.homefinance.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.service.*;
import ru.geekfactory.homefinance.service.dto.AccountServiceDto;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.EventsServiceDto;
import ru.geekfactory.homefinance.web.adapter.EventsDto;
import ru.geekfactory.homefinance.web.dto.EventsWebDto;

import java.util.Collection;

@Controller
@RequestMapping("/events")
public class EventsController {
    private final EventsService eventsService;
    private final CategoriesService categoriesService;
    private final AccountService accountService;
    private final SecureDataUserService secureDataUserService;

    @Autowired
    public EventsController(AccountService accountService,
                            EventsService eventsService,
                            CategoriesService categoriesService,
                            SecureDataUserService secureDataUserService) {

        this.eventsService = eventsService;
        this.accountService = accountService;
        this.categoriesService = categoriesService;
        this.secureDataUserService = secureDataUserService;
    }

    @GetMapping
    public String defaultList(Model model) {
        Collection<EventsServiceDto> allEvents = eventsService.findAll();
        Collection<AccountServiceDto> allAccounts = accountService.findAll();
        Collection<CategoriesServiceDto> allCategories = categoriesService.findAll();
        model.addAttribute("events", allEvents);
        model.addAttribute("accounts", allAccounts);
        model.addAttribute("categories", allCategories);
        model.addAttribute("user", secureDataUserService.getAuthentication().getName());

        return "events/events_list";
    }

    @GetMapping(value = "/{eventsId}")
    public String getTestData(@PathVariable Integer eventsId, Model model) {
        EventsServiceDto eventsModel = eventsService.findById(Long.valueOf(eventsId)).get();
        model.addAttribute("events", eventsModel);
        return "events/events_list";
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public String save(@RequestBody EventsWebDto eventsWebDto) {
        EventsDto eventsDto = new EventsDto(categoriesService, accountService);
        eventsService.save(eventsDto.EventsWebToEventsService(eventsWebDto));
        return "events/events_list";
    }

    @PutMapping(value = "/edit")
    public Boolean edit(@RequestBody EventsWebDto eventsWebDto) {
        EventsDto eventsDto = new EventsDto(categoriesService, accountService);
        return eventsService.update(eventsDto.EventsWebToEventsService(eventsWebDto));
    }

    @DeleteMapping(value = "/del")
    public Boolean remove(@RequestBody EventsWebDto eventsWebDto) {
        return eventsService.remove(eventsWebDto.getId());
    }

}
