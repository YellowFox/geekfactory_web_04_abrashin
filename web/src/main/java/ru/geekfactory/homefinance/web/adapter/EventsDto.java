package ru.geekfactory.homefinance.web.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import ru.geekfactory.homefinance.service.AccountService;
import ru.geekfactory.homefinance.service.CategoriesService;
import ru.geekfactory.homefinance.service.dto.CategoriesServiceDto;
import ru.geekfactory.homefinance.service.dto.EventsServiceDto;
import ru.geekfactory.homefinance.web.dto.EventsWebDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EventsDto {

    private CategoriesService categoriesService;
    private AccountService accountService;

    @Autowired
    public EventsDto(CategoriesService categoriesService,
                     AccountService accountService) {
        this.accountService = accountService;
        this.categoriesService = categoriesService;
    }

    public EventsServiceDto EventsWebToEventsService(EventsWebDto eventsWebDto) {
        EventsServiceDto eventsServiceDto = new EventsServiceDto();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(eventsWebDto.getDate(), formatter);
        if (eventsWebDto.getId() != null) {
            eventsServiceDto.setId(eventsWebDto.getId());
        } else {
            eventsServiceDto.setId(null);
        }
        if (eventsWebDto.getIsActive().equals("1")) {
            eventsServiceDto.setIsActive(true);
        } else {
            eventsServiceDto.setIsActive(false);
        }
        eventsServiceDto.setName(eventsWebDto.getName());
        eventsServiceDto.setAmount(eventsWebDto.getAmount());
        eventsServiceDto.setDate(dateTime);
        if (eventsWebDto.getAccount() != null) {
            eventsServiceDto.setAccountModel(accountService.findById(eventsWebDto.getAccount()).orElse(null));
        } else {
            eventsServiceDto.setAccountModel(null);
        }
        if (eventsWebDto.getCategories() != null && !eventsWebDto.getCategories().isEmpty()) {
            List<CategoriesServiceDto> categoriesModels = new ArrayList<>();
            for (Long cat : eventsWebDto.getCategories()) {
                if (cat != null) {
                    categoriesModels.add(categoriesService.findById(cat).orElse(null));
                } else {
                    eventsServiceDto.setCategories(null);
                    return eventsServiceDto;
                }
            }
            eventsServiceDto.setCategories(categoriesModels);
        } else {
            eventsServiceDto.setCategories(null);
        }
        return eventsServiceDto;

    }

}
