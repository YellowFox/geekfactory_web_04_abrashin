function add() {
    let object = {};
    let form = document.getElementById("dataAdd");
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let stringJson = JSON.stringify(object);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save/";
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function edit(id) {
    let object = {};
    let form = document.getElementById("dataEdit-" + id);
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let stringJson = JSON.stringify(object);
    alert(stringJson);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/edit/";
    xhr.open("PUT", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function editAccountUserPage(id) {
    let object = {};
    let form = document.getElementById("dataEdit-" + id);
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let stringJson = JSON.stringify(object);
    alert(stringJson);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/edit_account/";
    xhr.open("PUT", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
    } else {
        location.reload();
    }
}

function saveAccountUserPage() {
    let object = {};
    let form = document.getElementById("dataAdd");
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }

    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let stringJson = JSON.stringify(object);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save_account/";
    alert(stringJson);
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function editPassword(id) {
    let object = {};
    let form = document.getElementById("editPassword-" + id);
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });
    let stringJson = JSON.stringify(object);
    alert(stringJson);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/edit_password/";
    xhr.open("PUT", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function clone(id) {
    let object = {};
    let form = document.getElementById("dataEdit-" + id);
    let formData = new FormData(form);
    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let stringJson = JSON.stringify(object);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save/";
    alert(stringJson);
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function del(id) {
    let form = document.getElementById("dataDel-" + id);
    let formData = new FormData(form);
    let stringJson = JSON.stringify(Object.fromEntries(formData));
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/del/";
    alert(stringJson);
    alert(url);
    xhr.open("DELETE", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function addWithCategories() {
    let object = {};
    let form = document.getElementById("dataAdd");
    let formData = new FormData(form);
    formData.forEach(function (value, key) {
        object[key] = value;
    });
    if (validateForm(formData)) {
        return;
    }
    let categories = formData.getAll("categories");
    for (let i = 0; i < categories.entries(); i++) {
        if (categories[i] == null) {

        }
    }
    delete object["categories"];

    object["categories"] = categories;

    let stringJson = JSON.stringify(object);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save/";
    alert(stringJson);
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function editWithCategories(id) {
    let object = {};
    let form = document.getElementById("dataEdit-" + id);
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });

    let categories = formData.getAll("categories");
    delete object["categories"];

    object["categories"] = categories;

    let stringJson = JSON.stringify(object);
    alert(stringJson);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/edit/";
    xhr.open("PUT", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function cloneWithCategories(id) {
    let object = {};
    let form = document.getElementById("dataEdit-" + id);
    let formData = new FormData(form);
    formData.forEach(function (value, key) {
        object[key] = value;
    });
    let categories = formData.getAll("categories");
    delete object["categories"];

    object["categories"] = categories;

    let stringJson = JSON.stringify(Object.fromEntries(object));
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save/";
    alert(stringJson);
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        location.reload();
    }
}

function register() {
    let object = {};
    let form = document.getElementById("dataRegister");
    let formData = new FormData(form);
    if (validateForm(formData)) {
        return;
    }
    formData.forEach(function (value, key) {
        object[key] = value;
    });
    let stringJson = JSON.stringify(object);
    let xhr = new XMLHttpRequest();

    let url = window.location.pathname + "/save";
    alert(url);
    alert(stringJson);
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        //alert( xhr.status + ': ' + xhr.statusText );
    } else {
        //alert(JSON.parse(xhr.responseText));
        document.write(xhr.responseText);
        location.reload();
    }
}