function myFunction() {
    var inputName, inputSymbol, filterName, filterSymbol, table, tr, tdName, tdSymbol, i, txtValueName, txtValueSymbol;
    inputName = document.getElementById("search1");
    filterName = inputName.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        tdName = tr[i].getElementsByTagName("td")[0];
        if (tdName) {
            txtValueName = tdName.textContent || tdName.innerText;
            if (txtValueName.toUpperCase().indexOf(filterName) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
